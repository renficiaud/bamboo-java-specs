# Bamboo java specs for CI/CD

This repository contains a bunch of Bamboo Java specs that are currently running on my
personal Atlassian Bamboo CI instance. Atlassian kindly agreed to provide me with an
Bamboo license for open-source projects.


The Java specs span various projects, from pure Python to C++ and binding. the folder
`java-specs/common` contains reusable Java classes (see [this blog post](https://yayimorphology.org/bamboo-specs-project-sharing.html)
to see how to reuse those classes and organize your Java specs, and [this tag](https://www.yayimorphology.org/tag/bamboo.html) for related
articles).

Obviously those developments were made with my projects in mind, but I hope that you will find those
developments inspirational for your own projects. Do not hesitate to open an issue or send me
requests if you have any questions.

# Projects

## code-doc

The folder `java-specs/code_doc` contains the Java specs for building [code-doc](https://bitbucket.org/renficiaud/code_doc), which
is pure python. It has a Django test and a python linting/flake8 job.

## yayi

The folder `java-specs/yayi` contains the Java specs for building [yayi](https://bitbucket.org/renficiaud/yayi/), which is a
C++ project with Python3 bindings and various C++ dependencies. Currently the CI implements

* a python linter/flake job
* a C++ linting using `clang`
