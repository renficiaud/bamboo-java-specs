package org.yayimorphology.nan.codedoc;

import java.text.MessageFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.yayimorphology.nan.common.Python3Utils;
import org.yayimorphology.nan.common.Utils;
import org.yayimorphology.nan.common.Utils.BuildHost;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.notification.Notification;
import com.atlassian.bamboo.specs.api.builders.notification.NotificationRecipient;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.AllOtherPluginsConfiguration;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.ConcurrentBuilds;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.builders.notification.CommittersRecipient;
import com.atlassian.bamboo.specs.builders.notification.PlanFailedNotification;
import com.atlassian.bamboo.specs.builders.notification.UserRecipient;
import com.atlassian.bamboo.specs.builders.notification.WatchersRecipient;
import com.atlassian.bamboo.specs.builders.task.CheckoutItem;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.builders.task.TestParserTask;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;
import com.atlassian.bamboo.specs.builders.trigger.RepositoryPollingTrigger;
import com.atlassian.bamboo.specs.model.task.TestParserTaskProperties;
import com.atlassian.bamboo.specs.util.BambooServer;
import com.atlassian.bamboo.specs.util.MapBuilder;

/**
 * Plan configuration for Bamboo.
 *
 * @see <a href=
 *      "https://confluence.atlassian.com/display/BAMBOO/Bamboo+Specs">Bamboo
 *      Specs</a>
 */
@BambooSpec
public class PlanSpec {

    // this depends on the installation of bamboo
    Requirement python_requirement = new Requirement(
            "system.builder.command.python3.7");

    private static final String[] defaultFailureRecipients = {
            "raffi-admin"
    };

    abstract class CodedocJob {
        Utils.ConnectionType connection = Utils.ConnectionType.local;
        Utils.BuildHost host;
        Job bambooJob;

        String pipArguments;

        CodedocJob(String title, String key) {
            this.bambooJob = new Job(title, new BambooKey(key));;

        }

        void setJobRequirements() {
            List<Requirement> requirements = new ArrayList<>();

            requirements.add(python_requirement);

            bambooJob.requirements(requirements.toArray(new Requirement[0]));
        }

        abstract void createBuildTasks();

        void createBuildTasksInVirtualEnv() {
            String templateInstallVenv = Utils.multilineString(
                    "# saving the base directory",
                    "export base_dir=`pwd`",
                    "",
                    "# creating the virtual environment",
                    Python3Utils
                            .getPythonInBuildscript(
                                    Utils.BuildHost.linux,
                                    Utils.ConnectionType.local)
                            .replace("{", "'{'").replace("}", "'}'"),
                    "mkdir -p tmp",
                    "",
                    "$python3_binary -m venv tmp/venv_bamboo",
                    Python3Utils.getPythonVirtualenvActivationScript(
                            Utils.BuildHost.linux,
                            "tmp/venv_bamboo"),
                    "",
                    "# installing the project packages",
                    "cd {0}",
                    "pip install --upgrade pip",
                    "pip install {1}",
                    "",
                    "echo \"Installed python packages\"",
                    "pip list");

            // the above template does not indicate the `-r` switch on purpose
            // for pip
            String requirementToInstall = pipArguments;

            bambooJob.tasks(new ScriptTask()
                    .description("Creating a virtual environment")
                    .interpreterShell()
                    .inlineBody(Utils.multilineString(
                            Utils.get_shebang(BuildHost.linux),
                            "set -e",
                            Utils.get_go_to_working_directory(
                                    Utils.ConnectionType.local),
                            MessageFormat.format(templateInstallVenv, "src",
                                    requirementToInstall))));

            bambooJob.tasks(new ScriptTask()
                    .description("Running the build/tests command")
                    .interpreterShell()
                    .inlineBody(Utils.multilineString(
                            Utils.get_shebang(BuildHost.linux),
                            "set -e",
                            "",
                            "# saving the base directory",
                            "export base_dir=`pwd`",
                            "",
                            Python3Utils.getPythonVirtualenvActivationScript(
                                    Utils.BuildHost.linux,
                                    "tmp/venv_bamboo"),
                            "",
                            "echo \"#######################\"",
                            "echo \"building\"",
                            "echo \"#######################\"",
                            "cd src",
                            getPythonBuildScript())));

        }

        String getPythonBuildScript() {
            assert (false);
            return "";
        }
    }

    class Linting extends CodedocJob {
        Linting(String title, String key) {
            super(title, key);
            pipArguments = "-U autopep8 -U flake8";
        }

        void createBuildTasks() {
            createBuildTasksInVirtualEnv();
        }

        String getPythonBuildScript() {

            String buildScript = Utils.multilineString(
                    "has_failures=false",
                    "",
                    "# calling flake8",
                    "flake8 --statistics --config=tools/flake8_config --exit-zero --tee --output-file=$base_dir/tmp/flake8.out . 2>&1",
                    "if [[ -s $base_dir/tmp/flake8.out ]]; then",
                    "    (>&2 echo \"***** FLAKE FAILED *****\")",
                    "    (>&2 echo )",
                    "    (>&2 echo \"***** Report\")",
                    "    (>&2 cat $base_dir/tmp/flake8.out)",
                    "    has_failures=true",
                    "fi",
                    "",
                    "echo",
                    "echo",
                    "",
                    "# calling the linter",
                    "find . -type f \\( -name \"*.py\" \\) -not -path \"*/build*\" -exec \"autopep8\" --global-config tools/autopep8_config -i  {} +",
                    "# checking the output",
                    "if [[ `git status --porcelain` ]]; then",
                    "    (>&2 echo \"***** AUTOPEP8 FORMAT FAILED *****\")",
                    "    (>&2 echo )",
                    "    (>&2 echo \"***** Summary of changes\")",
                    "    # git diff --compact-summary not available on 16.04",
                    "    (>&2 git status --short)",
                    "    (>&2 echo )",
                    "    (>&2 echo \"***** Complete diff\")",
                    "    (>&2 git diff)",
                    "    has_failures=true",
                    "fi",
                    "",
                    "",
                    "if [[ \"$has_failures\" == true ]]; then",
                    "    exit 1",
                    "fi",
                    "");
            return buildScript;
        }
    }

    class DjangoBuild extends CodedocJob {
        DjangoBuild(String title, String key) {
            super(title, key);
            pipArguments = "-r requirements-dev.txt unittest-xml-reporting";
        }

        void createBuildTasks() {
            createBuildTasksInVirtualEnv();

            TestParserTask parse_tests = new TestParserTask(
                    TestParserTaskProperties.TestType.JUNIT)
                            .description("JUnit")
                            .resultDirectories("src/**/*.xml");

            bambooJob.finalTasks(parse_tests);
        }

        String getPythonBuildScript() {

            String buildScript = Utils.multilineString(
                    "# running the tests",
                    "python manage.py test -v 3 --testrunner 'xmlrunner.extra.djangotestrunner.XMLTestRunner'");
            return buildScript;
        }
    }

    Project project() {
        return new Project()
                .name("code-doc")
                .key(new BambooKey("CD"))
                .description("codedoc");
    }

    Plan createPlan() {
        Plan plan = new Plan(
                project(),
                "code-doc - continuous build",
                new BambooKey("CD"));

        plan.description("code-doc build plan");

        // configuring the main repository
        plan.linkedRepositories("codedoc@bitbucket");

        plan.pluginConfigurations(
                new ConcurrentBuilds()
                        .useSystemWideDefault(false),
                new AllOtherPluginsConfiguration()
                        .configuration(
                                new MapBuilder().put("custom", new MapBuilder()
                                        .put(
                                                "artifactHandlers.useCustomArtifactHandlers",
                                                "false")
                                        .put("buildExpiryConfig",
                                                new MapBuilder()
                                                        .put("duration", "2")
                                                        .put("period", "months")
                                                        .put("labelsToKeep", "")
                                                        .put("expiryTypeResult",
                                                                "true")
                                                        .put("buildsToKeep",
                                                                "1")
                                                        .put("enabled", "true")
                                                        .build())
                                        .build()).build()));

        // link to the builder repository

        // trigger
        plan.triggers(
                new RepositoryPollingTrigger()
                        .withPollingPeriod(Duration.ofSeconds(1200)));

        // branch management
        plan.planBranchManagement(
                new PlanBranchManagement()
                        .createForVcsBranch()
                        .delete(new BranchCleanup()
                                .whenRemovedFromRepositoryAfterDays(30)
                                .whenInactiveInRepositoryAfterDays(30))
                        .notificationForCommitters()
                        .triggerBuildsLikeParentPlan());

        // various notification configurations

        List<NotificationRecipient> failureNotificationRecipients = new ArrayList<>();
        for (final String recipient : defaultFailureRecipients) {
            failureNotificationRecipients.add(new UserRecipient(recipient));
        }
        failureNotificationRecipients.add(new CommittersRecipient());
        failureNotificationRecipients.add(new WatchersRecipient());

        plan.notifications(new Notification()
                .type(new PlanFailedNotification())
                .recipients(failureNotificationRecipients.toArray(
                        new NotificationRecipient[0])));

        CodedocJob lint = new Linting("linting", "LIN");
        CodedocJob django = new DjangoBuild("django", "DJ");

        CodedocJob allJobs[] = {lint, django};

        // checkout and various common properties
        for (CodedocJob currentJob : allJobs) {
            // all checkouts follow the same pattern
            VcsCheckoutTask checkout_task = new VcsCheckoutTask()
                    .description("Checkout Default Repository")
                    .checkoutItems(
                            new CheckoutItem()
                                    .defaultRepository()
                                    .path("src")) // checkout in /src folder,
                                                  // /tmp will be used by other
                                                  // things
                    .cleanCheckout(true);

            currentJob.bambooJob.cleanWorkingDirectory(true);
            currentJob.bambooJob.tasks(checkout_task);

            currentJob.setJobRequirements();

        }

        // build tasks
        for (CodedocJob currentJob : allJobs) {
            currentJob.createBuildTasks();
        }

        Stage stage_build = new Stage("Build and test");
        stage_build.jobs(lint.bambooJob, django.bambooJob);
        plan.stages(stage_build);

        return plan;
    }

    PlanPermissions createPlanPermission(PlanIdentifier planIdentifier) {

        PlanPermissions planPermission = new PlanPermissions(planIdentifier);
        planPermission.permissions(new Permissions()
                .userPermissions(
                        "raffi-admin",
                        PermissionType.VIEW,
                        PermissionType.BUILD,
                        PermissionType.CLONE,
                        PermissionType.EDIT,
                        PermissionType.ADMIN)
                .loggedInUserPermissions(
                        PermissionType.VIEW)

        );
        return planPermission;
    }

    /**
     * Run 'main' to publish your plan.
     */
    public static void main(String[] args) throws Exception {
        // by default credentials are read from the '.credentials' file
        BambooServer bambooServer = new BambooServer(
                "https://nan.yayimorphology.org/bamboo");

        Plan plan = new PlanSpec().createPlan();
        bambooServer.publish(plan);

        PlanPermissions planPermission = new PlanSpec()
                .createPlanPermission(plan.getIdentifier());
        bambooServer.publish(planPermission);
    }

};
