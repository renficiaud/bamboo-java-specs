package org.yayimorphology.nan.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.plan.Job;

public class DockerTest {
    @Test
    public void checkDockerBasic() {
        DockerUtils test = new DockerUtils(
                "this-is-the-name-prefix",
                "this-is-the-dockerfile-in-repo",
                new ArrayList<Pair<String, String>>() {
                    {
                        add(new ImmutablePair<String, String>("local-folder", "/some/container/folder"));
                    }
                });
        assertNotEquals(test.folder_mapping, null);
        assertEquals(test.folder_mapping.size(), 1);
    }

    @Test
    public void checkDockerSmoke() {
        DockerUtils test = new DockerUtils(
                "this-is-the-name-prefix",
                "this-is-the-dockerfile-in-repo",
                new ArrayList<Pair<String, String>>() {
                    {
                        add(new ImmutablePair<String, String>("local-folder", "/some/container/folder"));
                    }
                });

        Job simpleJob = new Job("simple", new BambooKey("SIM")).description("Check simple pipeline expansion");

        Job returnJob = test.prepareStartDockerPipeline(simpleJob);
        assertEquals(returnJob, simpleJob);
    }
}
