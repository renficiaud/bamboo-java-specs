package org.yayimorphology.nan.common;

import org.yayimorphology.nan.common.Utils.ConnectionType;

public class Python3Utils {

    /**
     * Returns a generic activation script for python virtual environments.
     *
     * @param venv_root_folder root folder of the virtual environment
     * @param host             host machine of the build
     */
    public static String getPythonVirtualenvActivationScript(
            String venv_root_folder) {
        assert (venv_root_folder != null);

        String activatePython3 = Utils.multilineString(
                Utils.get_ifthen_check_windows(),
                "    . " + venv_root_folder + "/Scripts/Activate",
                "else",
                "    . " + venv_root_folder + "/bin/activate",
                "fi");
        return activatePython3;
    }

    /**
     * Returns the location of python3 in a platform agnostic and safe way
     *
     * Inspects the content of the variable
     * ${bamboo.capability.system.builder.command.python3.7} without actually
     * accessing it directly but through environment variables. If that variable is
     * not defined on the host, then falls back to a `which python3`. Transforms the
     * content of the variable on Windows to be compatible with git-bash. For docker
     * type of jobs, as the host/agent variable should not propagate to the
     * container, evaluates to a `which python3`.
     *
     * @param host
     * @param connection
     * @return script snippet for getting python3 path. After the snippet execution,
     *         the environment variable "$python3_binary" will contain the path of
     *         python3.
     */
    public static String getPythonInBuildscript(
            ConnectionType connection) {

        if (connection == ConnectionType.docker) {
            return "export python3_binary=`which python3`";
        }

        String scriptGetPython3 = Utils.multilineString(
                "if [ -z ${bamboo_capability_system_builder_command_python3_7+x} ] ; then",
                "    export python3_binary=`which python3`",
                "else",
                "    " + Utils.get_ifthen_check_windows(),
                "        export python3_binary=$(cygpath.exe \"$bamboo_capability_system_builder_command_python3_7\")",
                "    else",
                "        export python3_binary=$bamboo_capability_system_builder_command_python3_7",
                "    fi",
                "fi");

        return scriptGetPython3;

    }

    /**
     * Returns the script for installing a python3 virtual environment. The script
     * automatically updates @c pip to its latest version.
     *
     * @param connection
     * @param venv_root_folder
     * @return
     */
    public static String scriptVirtualEnvInstall(
            ConnectionType connection,
            String venv_root_folder) {
        String commonVirtualEnvInstallationTaskTemplate = Utils.multilineString(
                "# saving the base directory",
                "export base_dir=`pwd`",
                "",
                "# creating the virtual environment",
                getPythonInBuildscript(connection),
                "mkdir -p " + venv_root_folder,
                "$python3_binary -m venv " + venv_root_folder,
                getPythonVirtualenvActivationScript(venv_root_folder),
                "python -m pip install --upgrade pip");

        return commonVirtualEnvInstallationTaskTemplate;
    }

}
