package org.yayimorphology.nan.common;

public class Utils {

    /// Indicates the type of machine on which the job is building
    public enum BuildHost {
        windows, osx, linux
    }

    public enum ConnectionType {
        local, docker
    }

    /// Returns the she-bang string in a platform agnostic way
    public static String get_shebang(BuildHost host) {
        if (host == BuildHost.windows) {
            return "#!C:/PROGRA~1/Git/bin/bash.exe";
        }
        return "#!/usr/bin/env bash";
    }

    /// Assumes that bin/bash is on PATH for all platforms
    public static String get_shebang() {
        return "#!${bamboo.capability.shebang_bash}";
    }

    public static String get_ifthen_check_windows() {
        return "if [[ \"$(uname -s | cut -c 1-10)\" == \"MINGW32_NT\" ]] || [[ \"$(uname -s | cut -c 1-10)\" == \"MINGW64_NT\" ]] || [[ \"$(uname -s | cut -c 1-7)\" == \"MSYS_NT\" ]] ; then";
    }

    /// Utility for joining several strings
    public static String multilineString(String... lines) {
        StringBuilder sb = new StringBuilder();
        for (String s : lines) {
            sb.append(s);
            sb.append('\n');
        }
        return sb.toString();
    }

    public static String echoInfoMessage(String info) {
        return multilineString(
                "echo",
                "echo \"********************************\"",
                "echo \"" + info + "\"",
                "echo \"********************************\"");
    }

}
