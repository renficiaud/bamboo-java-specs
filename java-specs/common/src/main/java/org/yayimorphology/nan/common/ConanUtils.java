package org.yayimorphology.nan.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.yayimorphology.nan.common.Utils.ConnectionType;

import com.atlassian.bamboo.specs.api.builders.plan.Plan;

public class ConanUtils {

    private ConnectionType connection;
    private Boolean clean_conan_cache = false;
    private String virtualEnvConan = "tmp/venv_conan";
    private String conanCachePrefix = "";

    /// Adds plan variable used for the automatic management of the cache
    public static Plan declarePlanVariables(Plan plan) {
        return plan;
    }

    public ConanUtils(ConnectionType connection) {
        this.connection = connection;
    }

    /**
     * Sets the prefix of the conan cache. This can be a Bamboo variable
     */
    public void setConanCachePrefix(String value) {
        conanCachePrefix = value;
    }

    private String getConanFolderInScripts() {
        if (connection == ConnectionType.docker) {
            // this is the same folder as mounted when docker is started.
            // this is part of the job setup.
            return "/cache/conan";
        }
        return conanCachePrefix + "/conan";
    }

    public String getConanVirtualEnvFolder() {
        return virtualEnvConan;
    }

    /// Returns a list of folder mappings between the host and the docker
    /// container.
    ///
    /// The call to this method is allowed only for docker connection types.
    public List<Pair<String, String>> getDockerFolderMapping() {
        // checkInstance(); // cannot check this as it is used for creating the
        // docker
        if (connection != ConnectionType.docker)
            throw new RuntimeException(
                    "Can be called only for jobs in docker environments");

        List<Pair<String, String>> output = new ArrayList<Pair<String, String>>();
        output.add(new ImmutablePair<String, String>(
                conanCachePrefix, "/cache"));

        return output;

    }

    /// Returns the script used for preparing the conan folder
    /// This is either executed on the host machine or via SSH for docker type
    /// of
    /// builds
    public String createConanPrepareTasks() {

        if (conanCachePrefix == "")
            throw new RuntimeException("Conan cache prefix not set");

        String script_skeleton = Utils.multilineString(
                Utils.get_shebang(),
                "set -e",
                "",
                "cd /data",
                // venv
                Utils.echoInfoMessage(
                        "creating and configuring the python virtual environment for conan"),
                Python3Utils.scriptVirtualEnvInstall(
                        this.connection,
                        getConanVirtualEnvFolder()),
                // this can be made optional and taken from the source folder
                // instead
                Utils.echoInfoMessage(
                        "adding conan to the virtual env"),
                "pip install -U conan",
                // conan folder
                "export CONAN_USER_HOME=\"" + getConanFolderInScripts() + "\"",
                "",
                Utils.echoInfoMessage("configuring conan"),
                "mkdir -p $CONAN_USER_HOME");

        // cleaning the cache requires conan to be there, so we need the virtual
        // environment
        // we also need to be on the right location (not the same for
        // docker/host jobs)
        if (clean_conan_cache) {
            script_skeleton += Utils.multilineString(
                    "",
                    "# Cleaning has been configured for the current build",
                    Utils.echoInfoMessage("clean conan cache"));

            // additional steps for cleaning up on Windows
            script_skeleton += Utils.multilineString(
                    Utils.get_ifthen_check_windows(),
                    "    conan remove -f '*/*@*/*' || echo \"No conan recipes to remove with '*/*@*/*'\"",
                    "    conan remove -f '*' || echo \"No conan recipes to remove with '*'\"",
                    "    rm -rf $CONAN_USER_HOME/.*",
                    "fi");

            script_skeleton += Utils.multilineString(
                    "rm -rf $CONAN_USER_HOME/*",
                    "rm -rf $CONAN_USER_HOME/.[!.]*",
                    "rm -rf $CONAN_USER_HOME/..?*");
        }

        script_skeleton += Utils.multilineString(
                "",
                "echo",
                Utils.echoInfoMessage("Conan cache folder content"),
                "ls -al $CONAN_USER_HOME",
                "",
                Utils.echoInfoMessage("Configuring conan"),
                "conan help > /dev/null",
                "echo conan version: - `conan --version` -",
                "conan remote remove conan-center || true",
                "echo conan remotes: - `conan remote list` -",
                "conan remove --locks",
                "",
                "mkdir -p " + getConanFolderInScripts() + "_download_cache",
                "conan config set storage.download_cache="
                        + getConanFolderInScripts() + "_download_cache",
                "",
                Utils.echoInfoMessage("Conan cache recipes"),
                "conan search \"*\"",
                "");

        return script_skeleton;
    }

    public String getConanHomeScriptlet() {
        return "export CONAN_USER_HOME=\"" + getConanFolderInScripts() + "\"";
    }

}
