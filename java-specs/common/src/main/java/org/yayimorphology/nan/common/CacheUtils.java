package org.yayimorphology.nan.common;

import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.builders.task.InjectVariablesTask;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.model.task.InjectVariablesScope;

/**
 * Cache utility
 *
 * This class provides utility function for handling the cache of the current
 * build. The cache can be used by various elements such as conan or ccache.
 *
 * A cache is relative to a reference version. Two revision of the code will
 * share the same cache if they share the same reference version. A revision
 * will use a different cache if the configuration of what is being cached
 * changed.
 *
 */
public class CacheUtils {
    private String cacheName = "";

    // File used to store the reference in case of a release branch
    private String cacheReferenceBranchFromFile = "cache_reference_branch.txt";

    // the internal file that is being generated for the bamboo dynamic
    // variables
    private final String cachePropertyFile = "tmp/local_cache_folders.prop";

    // Base of the cache folders. This can be an agent variable
    private String cacheFolderBase = "";
    private Boolean scriptAlreadyCalled = false;

    /// Adds plan variable used for the automatic management of the cache
    public static Plan declarePlanVariables(Plan plan) {
        plan.variables(
                new Variable("cache_automatic_isolation", "true"));

        return plan;
    }

    public CacheUtils() {
        if (scriptAlreadyCalled)
            throw new RuntimeException(
                    "Cannot configure the instance after having generated the jobs");
    }

    /**
     * Provide a base folder for the cache.
     *
     * This can be a Bamboo variable (global or a agent dependent).
     */
    public void setCacheFolderBase(String folderBase) {
        if (scriptAlreadyCalled)
            throw new RuntimeException(
                    "Cannot configure the instance after having generated the jobs");

        cacheFolderBase = folderBase;
    }

    /**
     * Provide a name for this cache
     *
     * The name can be logical or can refer to a Bamboo variable, such as
     * "${bamboo.buildKey}". For such a name, each job will have a different cache
     * folder, even if they are running on the same target machine.
     */
    public void setCacheName(String cacheName) {
        if (scriptAlreadyCalled)
            throw new RuntimeException(
                    "Cannot configure the instance after having generated the jobs");
        this.cacheName = cacheName;
    }

    /**
     * Indicates the file that is used to get the reference branch
     *
     * The "reference branch" is used for comparison: all the changes between the
     * current branch and the reference one are extracted. If those changes impact
     * the cache (which mean some meaningful files are modified), then the isolation
     * becomes active.
     *
     * This file is taken from the build/repository to enable various branches of
     * development that have an impact on the cache and that need isolation.
     */
    public void setCacheReferenceBranchFromFile(
            String cacheReferenceBranchFromFile) {

        if (scriptAlreadyCalled)
            throw new RuntimeException(
                    "Cannot configure the instance after having generated the jobs");
        this.cacheReferenceBranchFromFile = cacheReferenceBranchFromFile;
    }

    /**
     * Creates the scripts and injection tasks for automatic cache management of the
     * build.
     *
     * Injects the variables - ${bamboo.local_cache_folders.host_cache_folder}
     *
     * to the subsequent tasks of the current Job, and create the cache folder.
     */
    public Job createCacheFolderTasks(Job input) {
        scriptAlreadyCalled = true;
        // This script is always generated on the agent environment.
        String generate_dynamic_cache_values = Utils.multilineString(
                Utils.get_shebang(),
                "",
                "# check if platform is windows, if so we need special hashing of the paths to make",
                "# them shorter",
                "windows_tricks=false",
                Utils.get_ifthen_check_windows(),
                "    windows_tricks=true",
                "fi",
                "",
                "# generates dynamic values that will be injected to the build",
                "set -e",
                "",
                "mkdir -p tmp",
                "rm -f " + cachePropertyFile,
                "",
                "pushd src/ > /dev/null", // did not find a better way... needed
                                          // by git
                "# checks if this build requires isolation",
                "# by inspecting the diff wrt. base branch",
                "if [ -f " + cacheReferenceBranchFromFile + " ] ; then",
                "   base_branch=$(head -n 1 " + cacheReferenceBranchFromFile
                        + ")",
                "else",
                "   base_branch=master",
                "fi",
                "",
                "echo \"##\"",
                "echo \"## Base branch set to $base_branch\"",
                "echo \"##\"",
                "",
                "# lists the diff wrt. base branch",
                "list_files=$(git diff --name-only \"origin/${base_branch}\"..HEAD --)",
                "",
                "need_isolation=false",
                "if [[ $list_files == *\"conanfiles.py\"* ]] ; then",
                "    echo \"## Changes in the current branch require isolation\"",
                "    need_isolation=true",
                "fi",
                "",
                "suffix=\"_${base_branch}"
                        + (cacheName != "" ? "_" + cacheName : "")
                        + "\"",
                "if [[ $need_isolation == true ]] && [[ \"${bamboo.cache_automatic_isolation}\" != \"false\" ]] ; then",
                "    echo",
                "    echo \"## Isolating the cache for the build\"",
                "    echo",
                // we want this to be constant for a branch in a specific job,
                // we do not want to mix the caches of various jobs (buildKey)
                "    suffix=\"${suffix}_${bamboo.planRepository.1.branchName}\"",
                "fi",
                "");

        // we want more or less the same generated folder on various machines,
        // but we also
        // want to avoid mixing stuff always add the agent id such that the same
        // cache is not used by
        // several agents on the same machine (
        generate_dynamic_cache_values += Utils.multilineString(
                "# Adding the agentID to avoid collisions on concurrent builds",
                "suffix=\"${suffix}_${bamboo.agentId}\"",
                "");

        // (same hash computed even if running on several machines)
        // on windows, we hash the suffix such that the name is kept short
        // (path length issues)
        generate_dynamic_cache_values += Utils.multilineString(
                "if [[ $windows_tricks == true ]] ; then",
                "    # Windows build: hashing the suffix and taking a substring",
                // md5sum part of git-bash, on OSX it is md5
                "    suffix=_$( echo \"${suffix}\" | md5sum - | awk '{ printf $1 }' | cut -c 1-5 )",
                "fi",
                "");

        // script ending all platforms
        generate_dynamic_cache_values += Utils.multilineString(
                "popd > /dev/null", // see above
                "echo \"host_cache_folder=" + cacheFolderBase
                        + "/${suffix}/\" >> " + cachePropertyFile);

        // fixing slashes on windows
        generate_dynamic_cache_values += Utils.multilineString(
                // '/' and '\' are both transformed to '\\', the crazy
                // amount of '\\' is
                // - escaping of the bamboo injection -> \ -> \\
                // - escaping in the bash scripts that later will see \\ as
                // a single slash:
                // \\\\\\\\
                // - escaping in the current java code -> \\ -> \\\\
                "if [[ $windows_tricks == true ]] ; then",
                "    # windows: fixing forward/backward slashes",
                "    sed -r --in-place \"s/\\\\\\\\|\\//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/g\" "
                        + cachePropertyFile,
                "fi",
                "");

        generate_dynamic_cache_values += Utils.multilineString(
                "echo",
                "echo \"***********************************\"",
                "echo \"* " + cachePropertyFile + " content\"",
                "echo \"***********************************\"",
                "cat " + cachePropertyFile);

        input.tasks(
                new ScriptTask()
                        .description("Generate dynamic values for the caches")
                        .inlineBody(generate_dynamic_cache_values));

        input.tasks(
                new InjectVariablesTask()
                        .description(
                                "Inject cache dynamic variables to the build")
                        .path(cachePropertyFile)
                        .namespace("local_cache_folders")
                        .scope(InjectVariablesScope.LOCAL));

        input.tasks(
                new ScriptTask()
                        .description("Prepare cache folder")
                        .inlineBody(Utils.multilineString(
                                Utils.get_shebang(),
                                "set -e",
                                "",
                                "# creates the cache folders on the host.",
                                "mkdir -p ${bamboo.local_cache_folders.host_cache_folder}")));

        return input;
    }

    public String getCacheFolderBambooVariable() {
        return "${bamboo.local_cache_folders.host_cache_folder}";
    }

}
