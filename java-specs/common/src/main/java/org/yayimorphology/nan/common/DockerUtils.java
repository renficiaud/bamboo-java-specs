package org.yayimorphology.nan.common;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.builders.task.DockerBuildImageTask;
import com.atlassian.bamboo.specs.builders.task.DockerRunContainerTask;
import com.atlassian.bamboo.specs.builders.task.InjectVariablesTask;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.builders.task.SshTask;
import com.atlassian.bamboo.specs.model.task.InjectVariablesScope;

/*! Docker utility class
 *
 * This object creates the necessary jobs for
 *
 * * building a docker image from the repository
 * * building a docker image on top of the previous image such that it allows for SSH access
 * * mount specific number of folders from the host to the container
 * * waits for the container to start
 * * populates variables relative to the dynamic settings of the builds
 *
 */
public class DockerUtils {

    final private String target_image_name;
    private final String container_name;
    final private String dockerFile;
    final List<Pair<String, String>> folder_mapping;
    private Boolean link_to_detached;
    private final String dockerContextFolderInBuildTree;
    private final String container_env;
    private String additional_arguments;
    private String additional_build_arguments;

    // This is the password for SSH the docker container in an encrypted
    // fashion. It should correspond to the variable used in the
    // variable ${bamboo.bamboo_local_ssh_password}. This variable will be
    // declared for the plan in declarePlanVariables. The content can be changed
    // any time, but changing it will require a new build of the docker image (the
    // user to connect to will have another password).
    private static String ssh_password_encrypted = "BAMSCRT@0@0@A/K/RP5vFNEJyeXyqFrILC/waw+lkarehDPxrvyykvQ=";

    // Those 2 variables will be expanded to proper bamboo variables in scripts and
    // tasks
    public final static String ssh_password_variable = "bamboo_local_ssh_password";
    public final static String ssh_user_variable = "bamboo_local_ssh_user";

    /// Adds plan variable that are being used for the management of the docker
    /// tasks
    public static Plan declarePlanVariables(Plan plan) {
        plan.variables(
                // password used for SSH access to the docker runners
                new Variable(ssh_password_variable, ssh_password_encrypted),
                // user for the SSH access to the docker runners
                new Variable(ssh_user_variable, "bamboo_ci_user"));
        return plan;
    }

    /*
     * ! Construct the object from the docker file in the source tree
     *
     */
    public DockerUtils(
            String name_prefix,
            String dockerFile,
            List<Pair<String, String>> folder_mapping) {
        this(name_prefix, dockerFile, folder_mapping, "", false);
    }

    public DockerUtils(
            String name_prefix,
            String dockerFile,
            List<Pair<String, String>> folder_mapping,
            List<String> docker_additional_arguments) {
        this(name_prefix, dockerFile, folder_mapping, "", false);

        if (docker_additional_arguments != null) {
            additional_arguments = String.join(" ", docker_additional_arguments);
        }
    }

    public DockerUtils(
            String name_prefix,
            String dockerFile,
            List<Pair<String, String>> folder_mapping,
            String container_env,
            Boolean link_to_detached) {
        target_image_name = name_prefix + "-${bamboo.agentId}-${bamboo.local_docker_build.current_docker_jobname}";
        this.dockerFile = dockerFile;
        this.folder_mapping = folder_mapping;
        this.link_to_detached = link_to_detached;
        this.container_env = container_env;
        container_name = "bamboo-build-" + target_image_name.replace('/', '-');
        // cannot be prepended by ${bamboo.build.working.directory}/,
        // seems to be a bug in bamboo
        dockerContextFolderInBuildTree = "tmp/docker-build";

        additional_arguments = "";
        additional_build_arguments = "";
    }

    /// Returns the folder used in the build working directory that contains the
    /// internal docker context
    ///
    /// This may be used by client code in order to copy artifacts into that folder,
    /// and use `COPY` instructions in the Dockerfile.
    ///
    /// This property is immutable and available once the object has been
    /// constructed.
    public final String getDockerContextFolder() {
        return dockerContextFolderInBuildTree;
    }

    /// Indicates if the docker container should be linking to the previous detached
    /// containers of the current job.
    public final void linkToDetachedContainer(Boolean link_to_detached) {
        this.link_to_detached = link_to_detached;
    }

    public DockerUtils addDockerRunArguments(String dockerAdditionalArguments) {
        if (!additional_arguments.isEmpty()) {
            additional_arguments += " ";
        }
        additional_arguments += dockerAdditionalArguments.trim();
        return this;
    }

    public DockerUtils addDockerBuildArguments(String dockerAdditionalBuildArguments) {
        if (!additional_build_arguments.isEmpty()) {
            additional_build_arguments += " ";
        }
        additional_build_arguments += "--build-arg " + dockerAdditionalBuildArguments.trim();
        return this;
    }

    /**
     * Folder inside the container mounted to the bamboo working directory.
     *
     * @return the folder location inside the container
     */
    public static String getDockerWorkingDirectory() {
        return "/data";
    }

    /*
     * Creates a set of tasks that are preparing and running docker
     *
     * Adds the docker requirement to the job. The name of the image is built from
     * the name of the current job and the id of the agent. The name prefix
     */
    public Job prepareStartDockerPipeline(Job input) {

        // the image name prefix that will be built. We use the agentId to avoid
        // collision between several concurrent builds.
        // the name should be lowercase, it is processed by one of the injection
        // script below

        input.tasks(new ScriptTask()
                .description("Prepare docker build related tasks")
                .inlineBody(MessageFormat.format(
                        Utils.multilineString(
                                Utils.get_shebang().replace("{", "'{'").replace("}", "'}'"),
                                "# this script copies the Dockerfile.build from the repository to a dedicated place",
                                "# because we cannot set the file name from Bamboo directly",
                                "set -e",
                                "",
                                "mkdir -p {0}",
                                "cp '" + dockerFile + "' {0}/Dockerfile",
                                "",
                                "cat {0}/Dockerfile"),
                        dockerContextFolderInBuildTree)));

        // we generate the conan/ccache folders as a variable that we then reuse
        // in the
        input.tasks(new ScriptTask()
                .description("Generate dynamic values for the build")
                .inlineBody(Utils
                        .multilineString(
                                Utils.get_shebang(),
                                "# generates dynamic values that will be injected to the build",
                                "set -e",
                                "mkdir -p tmp",
                                "rm -f tmp/local_docker_build.prop",
                                "",
                                "echo \"current_user_id=`id -u`\" >> tmp/local_docker_build.prop",
                                "echo \"current_docker_jobname=${bamboo.buildKey}\" | tr '[:upper:]' '[:lower:]' >> tmp/local_docker_build.prop",
                                // Windows/certainly mac requires explicit publish command when running docker
                                // watch out the space before --publish
                                Utils.get_ifthen_check_windows(),
                                "    echo \"docker_expose_port= --publish 22:22\" >> tmp/local_docker_build.prop < /dev/null",
                                "else",
                                "    echo \"docker_expose_port=\" >> tmp/local_docker_build.prop < /dev/null",
                                "fi",

                                "",
                                "echo",
                                "echo \"**********************\"",
                                "echo \"local_docker_build.prop content\"",
                                "echo \"**********************\"",
                                "cat tmp/local_docker_build.prop")));

        input.tasks(new InjectVariablesTask()
                .description("Inject dynamic docker variables to the build")
                .path("tmp/local_docker_build.prop")
                .namespace("local_docker_build")
                .scope(InjectVariablesScope.LOCAL));

        // build the image that is in the temporary folder
        input.tasks(new DockerBuildImageTask()
                .description("Build repository image")
                .workingSubdirectory(dockerContextFolderInBuildTree)
                .imageName(target_image_name + ":latest")
                .useCache(true)
                .dockerfileInWorkingDir());

        // creates an image on top of this one with SSH enabled
        String ssh_dockerfile_build = Utils.multilineString(
                "FROM " + target_image_name + ":latest",
                "LABEL builder=bamboo",
                "");

        ssh_dockerfile_build += Utils.multilineString(
                "# Install ssh access",
                "RUN apt-get update && \\",
                "    apt-get install -y openssh-server",
                "RUN mkdir /var/run/sshd",
                "",
                "# Create build user and set password.",
                // we create the user with the same ID as the one running bamboo
                String.format(
                        "RUN useradd -m -d /home/${bamboo.%s} -s /bin/bash -u ${bamboo.local_docker_build.current_user_id} ${bamboo.%s}",
                        ssh_user_variable,
                        ssh_user_variable),
                // we set the password
                String.format("RUN echo '${bamboo.%s}:${bamboo.%s}' | chpasswd",
                        ssh_user_variable,
                        ssh_password_variable),
                "");

        ssh_dockerfile_build += Utils.multilineString(
                "EXPOSE 22",
                "WORKDIR /build",
                "# Runs the ssh daemon",
                "CMD /usr/sbin/sshd -D");

        input.tasks(new DockerBuildImageTask()
                .description("Build ssh-aware docker image")
                .imageName(target_image_name + "-ssh:latest")
                .useCache(true)
                .workingSubdirectory(dockerContextFolderInBuildTree) // not sending the whole context of the source
                // tree
                .dockerfile(ssh_dockerfile_build)
                .additionalArguments(additional_build_arguments));

        // the container name should not be clashing with any other container that may
        // not have been properly cleaned up. It should contain the agentId such that
        // those container can run in parallel on the same machine.
        // If the name starts with a common pattern, we can schedule a force stop in the
        // cleanup task.
        // the target_image_name contains already the agentId pattern.
        assert (target_image_name.indexOf("agentId") > -1);
        DockerRunContainerTask docker_task = new DockerRunContainerTask()
                .description("Start docker for building")
                .containerEnvironmentVariables(container_env)
                .environmentVariables("HOST_UID=$UID HOST_GID=$GID")
                .imageName(target_image_name + "-ssh:latest")
                .containerName(container_name)
                .serviceURLPattern("http://localhost:${docker.port}")
                .containerWorkingDirectory(getDockerWorkingDirectory())
                .clearVolumeMappings()
                .appendVolumeMapping("${bamboo.working.directory}", getDockerWorkingDirectory())
                .detachContainer(true)
                .linkToDetachedContainers(link_to_detached)
                .additionalArguments(additional_arguments + "${bamboo.local_docker_build.docker_expose_port}");

        for (final Pair<String, String> mapping : folder_mapping) {
            docker_task.appendVolumeMapping(
                    mapping.getLeft(),
                    mapping.getRight());
        }

        input.tasks(docker_task);

        // Gets the IP of the docker and injects it to the build (container name
        // = "bamboo-build-${bamboo.agentId}")
        input.tasks(new ScriptTask()
                .description("Generate dynamic values for the build")
                .inlineBody(Utils.multilineString(
                        Utils.get_shebang(),
                        "# Gets the IP address of the current running docker container",
                        "set -e",
                        "",
                        "mkdir -p tmp",
                        "rm -f tmp/local_docker.prop",
                        "",
                        // Windows (and mac certainly) per container IP addressing not working for linux
                        // containers: we connect to localhost instead
                        Utils.get_ifthen_check_windows(),
                        "    echo \"docker_ip=localhost\" >> tmp/local_docker.prop < /dev/null",
                        "else",
                        "    echo \"docker_ip=`docker inspect --format '{{ range .NetworkSettings.Networks }}{{ .IPAddress }}{{ end }}' "
                                + container_name +
                                "`\" >> tmp/local_docker.prop < /dev/null",
                        "fi",
                        "cat tmp/local_docker.prop")));

        // advertising for the network IP address in the variable
        input.tasks(new InjectVariablesTask()
                .description("Inject dynamic variables to the build")
                .path("tmp/local_docker.prop")
                .namespace("local_docker")
                .scope(InjectVariablesScope.LOCAL));

        // docker requirement
        input.requirements(new Requirement("system.docker.executable"));
        return input;
    }

    // This is a command that should be run on the host in order to get docker
    // started and then be able to connect to it via SSH
    public String get_docker_ready_script() {
        assert (container_name.length() > 0);
        return Utils.multilineString(
                // no "set -e" here
                Utils.get_shebang(),
                "",
                "check_task() {",
                "    echo \"Checking docker start #\" $1 \" ...\"",
                "    if [[ $(docker inspect --format '{{ .State.Running }}' $var_docker_instance_name) == 'true' ]]; then",
                "        echo \"Docker container $var_docker_instance_name running\"",
                "        /bin/true",
                "    else",
                "        echo \"Docker container $var_docker_instance_name NOT running\"",
                "        /bin/false",
                "    fi",
                "}",
                "",
                "check_sshd() {",
                "    echo \"Checking service availability #\" $1 \" ...\"",
                "    " + Utils.get_ifthen_check_windows(),
                "        echo netcat/nc not installed, sleeping instead",
                "        sleep 10",
                "    else",
                "        nc -zv ${bamboo.local_docker.docker_ip} 22",
                "    fi",
                "}",
                "",
                "var_docker_instance_name=" + container_name,
                "",
                "# checking the availability of the service",
                "remaining_iterations=10",
                "check_task $remaining_iterations",
                "res=$?",
                "while [ $res != 0 ] && [ $remaining_iterations -gt 0 ] ; do",
                "    sleep 3",
                "    check_task $remaining_iterations",
                "    res=$?",
                "    remaining_iterations=$(($remaining_iterations - 1))",
                "done",
                "if [ $remaining_iterations -le 0 ] ; then",
                "    (>&2 echo \"Docker failed to start after 10 iterations!\")",
                "    (>&2 echo)",
                "    (>&2 echo \"Docker logs of the container:\")",
                "    (>&2 docker logs $var_docker_instance_name)",
                "    (>&2 echo)",
                "    (>&2 echo \"Listing the running containers:\")",
                "    (>&2 docker ps)",
                "    (>&2 echo)",
                "    (>&2 echo \"Listing the docker images:\")",
                "    (>&2 docker images)",
                "    exit 1",
                "else",
                "    echo \"Docker started and all services up ... \"",
                "fi",
                "",
                "# inspecting the port 22 for ssh startup",
                "remaining_iterations=10",
                "check_sshd $remaining_iterations",
                "res=$?",
                "while [ $res != 0 ] && [ $remaining_iterations -gt 0 ] ; do",
                "    sleep 3",
                "    check_sshd $remaining_iterations",
                "    res=$?",
                "    remaining_iterations=$(($remaining_iterations - 1))",
                "done",
                "if [ $remaining_iterations -le 0 ] ; then",
                "    (>&2 echo \"SSH failed to start after 10 iterations!\")",
                "    (>&2 echo)",
                "    (>&2 echo \"Docker logs of the container:\")",
                "    (>&2 docker logs $var_docker_instance_name)",
                "    (>&2 echo)",
                "    (>&2 echo \"Listing the running containers:\")",
                "    (>&2 docker ps)",
                "    (>&2 echo)",
                "    (>&2 echo \"Listing the docker images:\")",
                "    (>&2 docker images)",
                "    exit 1",
                "else",
                "    echo \"SSH started and up ... \"",
                "fi",
                "",
                "exit 0");
    }

    /// Runs the script in the context of docker
    public Job addContainerTask(Job input, String title, String script) {
        input.tasks(new SshTask()
                .description(title)
                .host("${bamboo.local_docker.docker_ip}")
                .username(String.format("${bamboo.%s}", ssh_user_variable))
                .authenticateWithPassword(ssh_password_encrypted)
                .command(script));

        return input;
    }

    /// Runs the script in the context of docker as a final task
    public Job addContainerFinalTask(Job input, String title, String script) {
        input.finalTasks(new SshTask()
                .description(title)
                .host("${bamboo.local_docker.docker_ip}")
                .username(String.format("${bamboo.%s}", ssh_user_variable))
                .authenticateWithPassword(ssh_password_encrypted)
                .command(script));

        return input;
    }

    public String getContainerName() {
        return container_name;
    }

    public String getContainerNameAsEnvironmentVariable() {
        return container_name.replace('.', '_');
    }

}
