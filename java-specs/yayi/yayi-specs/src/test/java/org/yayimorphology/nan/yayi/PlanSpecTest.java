package org.yayimorphology.nan.yayi;

import org.junit.Test;

import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;

public class PlanSpecTest {
    @Test
    public void checkYourPlanOffline() {
        Plan plan = new PlanSpec().createPlan();
        PlanPermissions permissions = PlanSpec.planPermission(plan.getIdentifier());

        EntityPropertiesBuilders.build(plan);
        EntityPropertiesBuilders.build(permissions);
    }
}
