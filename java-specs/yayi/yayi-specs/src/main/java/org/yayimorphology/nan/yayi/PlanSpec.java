package org.yayimorphology.nan.yayi;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.yayimorphology.nan.common.CacheUtils;
import org.yayimorphology.nan.common.ConanUtils;
import org.yayimorphology.nan.common.DockerUtils;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsIdentifier;
import com.atlassian.bamboo.specs.api.builders.notification.Notification;
import com.atlassian.bamboo.specs.api.builders.notification.NotificationRecipient;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.AllOtherPluginsConfiguration;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.ConcurrentBuilds;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.repository.VcsChangeDetection;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.builders.notification.CommittersRecipient;
import com.atlassian.bamboo.specs.builders.notification.PlanFailedNotification;
import com.atlassian.bamboo.specs.builders.notification.UserRecipient;
import com.atlassian.bamboo.specs.builders.notification.WatchersRecipient;
import com.atlassian.bamboo.specs.builders.repository.bitbucket.cloud.BitbucketCloudRepository;
import com.atlassian.bamboo.specs.builders.repository.git.GitRepository;
import com.atlassian.bamboo.specs.builders.repository.viewer.BitbucketCloudRepositoryViewer;
import com.atlassian.bamboo.specs.builders.task.CheckoutItem;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;
import com.atlassian.bamboo.specs.builders.trigger.RepositoryPollingTrigger;
import com.atlassian.bamboo.specs.util.BambooServer;
import com.atlassian.bamboo.specs.util.MapBuilder;

/**
 * Plan configuration for Bamboo.
 *
 * @see <a href=
 *      "https://confluence.atlassian.com/display/BAMBOO/Bamboo+Specs">Bamboo
 *      Specs</a>
 */
@BambooSpec
public class PlanSpec {

    String checkoutFolder = "src";

    Requirement python_requirement = new Requirement(
            "system.builder.command.python3.7");
    Requirement cmake_requirement = new Requirement(
            "system.builder.command.cmake");
    Requirement doxygen_requirement = new Requirement("doxygen");
    String boost_test_run_environments = "BOOST_TEST_LOGGER=HRF,test_suite:JUNIT,error";

    private static final String[] defaultFailureRecipients = {
            "raffi-admin"
    };

    Project project() {
        return new Project()
                .name("yayi")
                .key(new BambooKey("YY"))
                .description("Yayi related builds");
    }

    Plan createPlan() {
        Plan plan = new Plan(
                project(),
                "yayi - continuous build",
                new BambooKey("YCB"));

        // declare plan variables
        CacheUtils.declarePlanVariables(plan);
        DockerUtils.declarePlanVariables(plan);
        ConanUtils.declarePlanVariables(plan);

        plan.description("Yayi build plan");

        BitbucketCloudRepository yayiRepo = new BitbucketCloudRepository()
                .name("yayi@bitbucket")
                .repositoryViewer(new BitbucketCloudRepositoryViewer())
                .repositorySlug("renficiaud", "yayi")
                .accountAuthentication(new SharedCredentialsIdentifier(
                        "raffienficiaud-bitbucket"))
                .checkoutAuthentication(new SharedCredentialsIdentifier(
                        "accesskey_perso_bamboo"))
                .branch("master")
                .remoteAgentCacheEnabled(true)
                .verboseLogs(true)
                .fetchWholeRepository(true)
                .changeDetection(new VcsChangeDetection());

        GitRepository conanIndex = new GitRepository()
                .name("conan-index@github")
                .description("conan index used to build conan packages")
                .remoteAgentCacheEnabled(true)
                .withoutAuthentication()
                .url("https://github.com/conan-io/conan-center-index");

        plan.planRepositories(
                yayiRepo, // the main repository
                conanIndex);

        plan.pluginConfigurations(
                new ConcurrentBuilds()
                        .useSystemWideDefault(false),
                new AllOtherPluginsConfiguration()
                        .configuration(
                                new MapBuilder().put("custom", new MapBuilder()
                                        .put(
                                                "artifactHandlers.useCustomArtifactHandlers",
                                                "false")
                                        .put("buildExpiryConfig",
                                                new MapBuilder()
                                                        .put("duration", "2")
                                                        .put("period", "months")
                                                        .put("labelsToKeep", "")
                                                        .put("expiryTypeResult",
                                                                "true")
                                                        .put("buildsToKeep",
                                                                "1")
                                                        .put("enabled", "true")
                                                        .build())
                                        .build()).build()));

        // link to the builder repository

        // trigger
        plan.triggers(
                new RepositoryPollingTrigger()
                        .withPollingPeriod(Duration.ofSeconds(1200))
                        .description("changes on yayi every 20min")
                        .selectedTriggeringRepositories(
                                yayiRepo.getIdentifier()));

        // branch management
        plan.planBranchManagement(
                new PlanBranchManagement()
                        .createForVcsBranch()
                        .delete(new BranchCleanup()
                                .whenRemovedFromRepositoryAfterDays(30)
                                .whenInactiveInRepositoryAfterDays(30))
                        .notificationForCommitters()
                        .triggerBuildsLikeParentPlan());

        // various notification configurations

        List<NotificationRecipient> failureNotificationRecipients = new ArrayList<>();
        for (final String recipient : defaultFailureRecipients) {
            failureNotificationRecipients.add(new UserRecipient(recipient));
        }
        failureNotificationRecipients.add(new CommittersRecipient());
        failureNotificationRecipients.add(new WatchersRecipient());

        plan.notifications(new Notification()
                .type(new PlanFailedNotification())
                .recipients(failureNotificationRecipients.toArray(
                        new NotificationRecipient[0])));

        YayiJob pythonLintingJob = new PythonLintingJob(
                new Job("python linting", new BambooKey("PL")));
        YayiJob clangLintingJob = new ClangLintingJob(
                new Job("clang linting", new BambooKey("CL")));
        YayiJob clangBuildJob = new BuildClangDockerJob(
                // the name will be overridden in the instance
                new Job("clang template-override", new BambooKey("CB")));
        YayiJob allJobs[] = {
                pythonLintingJob,
                clangLintingJob,
                clangBuildJob
        };

        for (YayiJob currentJob : allJobs) {
            currentJob.bambooJob.cleanWorkingDirectory(true);
            currentJob.setJobRequirements();
            currentJob.setSourcecodeFolder(checkoutFolder);
            currentJob.configureBuild();
        }

        // create the tasks for each jobs
        for (YayiJob currentJob : allJobs) {
            // all checkouts follow the same pattern
            // checkout code

            List<CheckoutItem> listCheckout = new ArrayList<CheckoutItem>() {
                private static final long serialVersionUID = 1L;

                {
                    add(new CheckoutItem()
                            .defaultRepository()
                            .path(checkoutFolder));
                }
            };

            if (currentJob.requiresConan) {
                listCheckout.add(new CheckoutItem()
                        .repository("conan-index@github")
                        .path(checkoutFolder + "_conan"));

            }

            VcsCheckoutTask checkout_task = new VcsCheckoutTask()
                    .description("Checkout Default Repository")
                    .checkoutItems(
                            listCheckout.stream().toArray(CheckoutItem[]::new))
                    .cleanCheckout(true);

            currentJob.bambooJob.tasks(checkout_task);

            // configure build
            currentJob.build();
        }

        /*
         * jobMac.pluginConfigurations(new AllOtherPluginsConfiguration()
         * .configuration(new MapBuilder() .put("custom", new MapBuilder() .put("auto",
         * new MapBuilder() .put("regex", "") .put("label", "") .build())
         * .put("buildHangingConfig.enabled", "false") .put("ncover.path", "")
         * .put("clover", new MapBuilder() .put("path", "") .put("license", "")
         * .put("integration", "custom") .put("useLocalLicenseKey", "true") .build())
         * .build()) .build()));
         */
        // Artifacts declaration
        Artifact package_osx = new Artifact().name("Package OSX")
                .copyPattern("*Darwin.tar.bz2")
                .location("temporary/cmake_project/release").shared(true);
        Artifact doc_html = new Artifact().name("DoxygenHTML")
                .copyPattern("**/*.*")
                .location(
                        "temporary/cmake_project/release/documentation/doxygen/html/")
                .shared(true);
        Artifact doc_html_tar = new Artifact().name("DoxygenTar")
                .copyPattern("*.tar.bz2")
                .location(
                        "temporary/cmake_project/release/documentation/doxygen/")
                .shared(true);

        // OSX build with tar of the documentation
        String osx_build_script = "-m bamboo_compilation yayi "
                + "cmake/all build/debug ctest/debug build/release build/release/Doxygen ctest/release "
                + "--platform osx "
                + "--cmake_executable=${bamboo.capability.system.builder.command.cmake}";

        String tar_doxygen_script = ""
                + "cd temporary/cmake_project/release/documentation/doxygen/html/\n"
                + "tar --exclude=.svn --exclude=.git -v -j -c -f ../yayi-r${bamboo.planRepository.revision}.tar.bz2 .\n"
                + "ls -al ..";

        //
        // Stages
        Stage stageLint = new Stage("Linting");
        stageLint.jobs(
                pythonLintingJob.bambooJob,
                clangLintingJob.bambooJob);

        Stage stageBuild = new Stage("Build and test");
        stageBuild.jobs(
                clangBuildJob.bambooJob);

        plan.stages(stageLint, stageBuild);

        return plan;
    }

    /**
     * Authorizations for the enterprise projects
     *
     * @return list of failure recipients
     */
    public static PlanPermissions planPermission(
            final PlanIdentifier identifier) {

        final String adminUsers[] = { "raffi-admin" };

        final Permissions allPermissions = new Permissions();

        for (final String user : adminUsers) {
            allPermissions.userPermissions(user,
                    PermissionType.EDIT,
                    PermissionType.VIEW,
                    PermissionType.ADMIN,
                    PermissionType.CLONE,
                    PermissionType.BUILD);

        }
        // allPermissions.groupPermissions("bamboo-users", PermissionType.VIEW);

        final PlanPermissions planPermission = new PlanPermissions(identifier)
                .permissions(allPermissions);
        return planPermission;
    }

    /**
     * Run 'main' to publish your plan.
     */
    public static void main(String[] args) throws Exception {
        // by default credentials are read from the '.credentials' file
        BambooServer bambooServer = new BambooServer(
                "https://nan.yayimorphology.org/bamboo");

        final PlanSpec planSpec = new PlanSpec();

        final Plan plan = planSpec.createPlan();
        bambooServer.publish(plan);

        final PlanPermissions planPermission = PlanSpec
                .planPermission(plan.getIdentifier());
        bambooServer.publish(planPermission);

    }

};
