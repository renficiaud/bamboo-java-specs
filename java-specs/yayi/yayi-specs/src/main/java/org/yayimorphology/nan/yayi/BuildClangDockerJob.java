package org.yayimorphology.nan.yayi;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.yayimorphology.nan.common.DockerUtils;
import org.yayimorphology.nan.common.Python3Utils;
import org.yayimorphology.nan.common.Utils;

import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.builders.task.DockerBuildImageTask;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.builders.task.TestParserTask;
import com.atlassian.bamboo.specs.model.task.TestParserTaskProperties;

public class BuildClangDockerJob extends YayiJob {

    public final static String clangVersion = "10.0.0";
    public final static String cmakeVersion = "3.18.3";
    private final static String imageNameBase = "bamboo-build-with-clang";
    private final static String dockerFolderBuildCMakePython = "tmp/build-cmake-python";

    private final String clangMajor = clangVersion.split("\\.")[0];

    public BuildClangDockerJob(Job bambooJob) {
        super(bambooJob);
        // change the name to reflect the version
        bambooJob.name("clang-" + clangMajor);
        requiresDocker = true;
        // this is the last docker we create and that goes into the super.docker
        // object
        dockerFile = dockerFolderBuildCMakePython + "/Dockerfile";
    }

    @Override
    public void configureBuild() {
        // caching the pip folder
        // dockerMountLocation.add(new ImmutablePair<String, String>(
        // this.cacheUtils.getCacheFolderBambooVariable() + "/pip_cache",
        // String.format("/home/${bamboo.%1$s}/.cache/pip",
        // DockerUtils.ssh_user_variable)));
        super.configureBuild();
        requiresConan = true;
    }

    @Override
    public void build() {

        ClangUtils clangUtils = new ClangUtils(clangVersion);
        this.cacheUtils.createCacheFolderTasks(bambooJob); // needed?

        // 1- we build a docker file that contains clang (should reuse some
        // existing
        // cache)
        String dockerFolderBuildClang = "tmp/build-clang";
        String scriptDockerCreate = clangUtils.bashScriptToGetDockerFile(
                dockerFolderBuildClang + "/Dockerfile");

        this.bambooJob.tasks(
                new ScriptTask()
                        .description("Create Dockerfile for clang")
                        .interpreterShell()
                        .inlineBody(
                                Utils.multilineString(
                                        Utils.get_shebang(), // should be available on PATH
                                        "set -e",
                                        "",
                                        "mkdir -p " + dockerFolderBuildClang,
                                        scriptDockerCreate)));

        this.bambooJob.tasks(new DockerBuildImageTask()
                .description("Build docker with clang " + clangVersion)
                .workingSubdirectory(dockerFolderBuildClang)
                .imageName(imageNameBase + "-clang-install:latest")
                .useCache(true)
                .dockerfileInWorkingDir());

        // 2- we build a docker file on top of that one that contains cmake and
        // python3
        String dockerFileCmakePythonContent = Utils.multilineString(
                "FROM " + imageNameBase + "-clang-install:latest",
                //
                // installs python tools
                "RUN apt-get update \\",
                "    && apt-get install -y \\",
                "         wget \\",
                "         python3 \\",
                "         python3-dev \\",
                "         python3-pip \\",
                "         python3-venv \\",
                "    && rm -rf /var/lib/apt/lists/*",
                "",
                //
                // installs cmake
                "RUN apt-get update \\",
                "    && apt-get install -y \\",
                "          apt-transport-https \\",
                "          ca-certificates \\",
                "          gnupg \\",
                "          software-properties-common \\",
                "          lsb-release \\",
                "    && rm -rf /var/lib/apt/lists/*",
                "",
                //
                // installs cmake ppa
                // "RUN wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc
                // 2>/dev/null | gpg --dearmor - | tee /etc/apt/trusted.gpg.d/kitware.gpg
                // >/dev/null",
                // "RUN apt-add-repository \"deb https://apt.kitware.com/ubuntu/
                // \"\\$(lsb_release -c -s)\" main\" \\",
                // " && apt-get update \\",
                // " && apt-get install -y cmake \\",
                // " && apt-cache show cmake \\",
                // " && rm -rf /var/lib/apt/lists/*",
                //
                // install build tools
                "RUN apt-get update \\",
                "    && apt-get install -y \\",
                "         ninja-build \\",
                "         build-essential \\",
                "         make \\",
                "    && rm -rf /var/lib/apt/lists/*",
                "",
                //
                // install cmake by building it, needed for arm or if the
                // previous command does not give the needed version of cmake
                String.format(
                        Utils.multilineString(
                                "RUN apt-get update \\",
                                "    && apt-get install -y \\",
                                "        libssl-dev \\",
                                "    && rm -rf /var/lib/apt/lists/* \\",
                                "    && cd /tmp \\",
                                "    && wget https://github.com/Kitware/CMake/releases/download/v%1$s/cmake-%1$s.tar.gz \\",
                                "    && tar xzf cmake-%1$s.tar.gz \\",
                                "    && cd cmake-%1$s \\",
                                "    && env \\",
                                "       CC=/usr/local/clang-%2$s/bin/clang \\",
                                "       CXX=/usr/local/clang-%2$s/bin/clang++ \\",
                                "       ./bootstrap -- -DCMAKE_BUILD_TYPE:STRING=Release \\",
                                "    && make -j2 \\",
                                "    && make install \\",
                                "    && rm -rf /tmp/cmake*"),
                        BuildClangDockerJob.cmakeVersion,
                        BuildClangDockerJob.clangVersion));

        this.bambooJob.tasks(
                new ScriptTask()
                        .description("Create Dockerfile for cmake and python")
                        .interpreterShell()
                        .inlineBody(
                                Utils.multilineString(
                                        Utils.get_shebang(), // should be available on PATH
                                        "set -e",
                                        "",
                                        "mkdir -p "
                                                + dockerFolderBuildCMakePython,
                                        "cat <<EOF > " + dockerFile,
                                        dockerFileCmakePythonContent,
                                        "EOF",
                                        "",
                                        Utils.echoInfoMessage(
                                                "Docker file content"),
                                        "cat " + dockerFile)));

        // now able to construct the docker
        this.docker.prepareStartDockerPipeline(this.bambooJob);
        this.bambooJob.tasks(
                new ScriptTask()
                        .description("Wait for docker to be ready")
                        .interpreterShell()
                        .inlineBody(this.docker.get_docker_ready_script()));

        String script = Utils.multilineString(
                clangUtils.bashScriptToGetDockerFilePATHSettingsInDocker(),
                "",
                "cd " + this.getSourcecodeFolder(),
                "",
                Utils.echoInfoMessage("Tools info"),
                "clang --version",
                "cmake --version");

        addTask("Run tools info", script);

        String scriptPipCache = Utils.multilineString(
                clangUtils.bashScriptToGetDockerFilePATHSettingsInDocker(),
                "",
                String.format("mkdir -p /home/${bamboo.%1$s}/.cache/",
                        DockerUtils.ssh_user_variable),
                String.format("mkdir -p /cache/pip_yayi"),
                String.format(
                        "ln -s /cache/pip_yayi /home/${bamboo.%1$s}/.cache/pip",
                        DockerUtils.ssh_user_variable));

        addTask("Cache pip folder", scriptPipCache);

        // prepare conan stuff
        addTask("Installs and configures conan",
                this.conanUtils.createConanPrepareTasks());

        List<Pair<String, String>> dependencies = new ArrayList<Pair<String, String>>();

        dependencies.add(new ImmutablePair<String, String>("boost", "1.73.0"));
        dependencies.add(new ImmutablePair<String, String>("libjpeg", "9d"));
        dependencies.add(new ImmutablePair<String, String>("libpng", "1.6.37"));
        dependencies.add(new ImmutablePair<String, String>("hdf5", "1.12.0"));
        dependencies.add(new ImmutablePair<String, String>("libtiff", "4.1.0"));
        dependencies.add(new ImmutablePair<String, String>("jbig", "20160605"));
        dependencies.add(new ImmutablePair<String, String>("zstd", "1.4.5"));
        dependencies
                .add(new ImmutablePair<String, String>("xz_utils", "5.2.5"));
        dependencies
                .add(new ImmutablePair<String, String>("pybind11", "2.4.3"));

        String scriptRegisterConanPackages = Utils.multilineString(
                clangUtils.bashScriptToGetDockerFilePATHSettingsInDocker(),
                conanUtils.getConanHomeScriptlet(),
                "",
                "export base_dir=`pwd`",
                Python3Utils.getPythonVirtualenvActivationScript(
                        this.conanUtils.getConanVirtualEnvFolder()),
                "",
                "cd " + this.getSourcecodeFolder() + "_conan");

        for (Pair<String, String> current : dependencies) {
            scriptRegisterConanPackages = Utils.multilineString(
                    scriptRegisterConanPackages,
                    // final @ to avoid user/channel
                    String.format(
                            "conan export recipes/%1$s/all %1$s/%2$s@",
                            current.getLeft(), current.getRight()));
        }

        scriptRegisterConanPackages = Utils.multilineString(
                scriptRegisterConanPackages,
                // final @ to avoid user/channel
                "conan export recipes/zlib/1.2.11 zlib/1.2.11@",
                "conan export recipes/b2/standard b2/4.2.0@",
                "conan search \"*\"");

        addTask("Register conan packages", scriptRegisterConanPackages);

        String scriptBuild = Utils.multilineString(
                clangUtils.bashScriptToGetDockerFilePATHSettingsInDocker(),
                "export base_dir=`pwd`",
                Python3Utils.getPythonVirtualenvActivationScript(
                        this.conanUtils.getConanVirtualEnvFolder()),
                "",
                conanUtils.getConanHomeScriptlet(),
                Utils.echoInfoMessage(
                        "Conan cache content"),
                "conan search \"*\"",
                "",
                Utils.echoInfoMessage(
                        "Install project's requirements..."),
                "cd ${base_dir}/" + this.getSourcecodeFolder(),
                "pip install -r requirements-dev.txt",
                "",
                Utils.echoInfoMessage(
                        "Preparing conan profile..."),
                "mkdir -p ${base_dir}/tmp/build",
                "cd ${base_dir}/tmp/build",
                "",
                "export profile_filename=`pwd`/clang-" + clangMajor,
                "export clang_location=$(cd $(dirname \"`which clang`\")/.. ; pwd -P ; cd - >/dev/null )",
                "conan profile new --detect $profile_filename || echo \"Profile exists\"",
                "conan profile update settings.compiler=clang $profile_filename",
                "conan profile update settings.compiler.version=10 $profile_filename",
                "conan profile update settings.compiler.libcxx=libc++ $profile_filename",
                "conan profile update env.CC=clang $profile_filename",
                "conan profile update env.CXX=clang++ $profile_filename",
                "conan profile update env.CFLAGS=\"-I$clang_location/include/c++/v1/\" $profile_filename",
                "conan profile update env.CXXFLAGS=\"-I$clang_location/include/c++/v1/ -nostdinc++\" $profile_filename",
                "conan profile update env.LDFLAGS=\"-L$clang_location/lib -stdlib=libc++ -Wl,-rpath,$clang_location/lib\" $profile_filename",
                "",
                Utils.echoInfoMessage(
                        "Building conan packages..."),
                "mkdir -p ${base_dir}/tmp/build",
                "conan install --build=missing --profile=`pwd`/clang-10 ${base_dir}/"
                        + this.getSourcecodeFolder(),
                "",
                Utils.echoInfoMessage(
                        "Configuring project..."),
                "export YAYI_PYTHON_TESTS_PYTEST=1",
                "export YAYI_TESTS_OUTPUTS_JUNIT=1",
                "export CC=clang",
                "export CXX=clang++",
                "export CFLAGS=\"-I$clang_location/include/c++/v1/\"", // -nostdinc cannot be used and does not make
                                                                       // sense
                "export CXXFLAGS=\"-I$clang_location/include/c++/v1/ -nostdinc++\"",
                "export LDFLAGS=\"-L$clang_location/lib -Wl,-rpath,$clang_location/lib\"",
                "cmake \\",
                "  -GNinja \\",
                "  -DUSE_CONAN=ON \\",
                "  -DEXTERNAL_REVISION=\"${bamboo.planRepository.1.revision}\" \\",
                "  -DENABLE_HDF5=ON \\",
                "  -DENABLE_NUMPY=OFF \\", // Jira YAYI-18
                "  -DPython3_EXECUTABLE=`which python` \\", // TODO find a better way
                "  ${base_dir}/" + this.getSourcecodeFolder(),
                "",
                Utils.echoInfoMessage(
                        "Building project"),
                "cmake --build .",
                "",
                Utils.echoInfoMessage(
                        "Running tests"),
                "ctest -C Debug --output-on-failure");

        addTask("Build", scriptBuild);

        // final parse task
        TestParserTask testParseTask = new TestParserTask(
                TestParserTaskProperties.TestType.JUNIT)
                        .description("JUnit")
                        .resultDirectories("tmp/build/**/test*-yayi-*.xml");

        this.bambooJob.finalTasks(testParseTask);
    }

}
