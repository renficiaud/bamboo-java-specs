package org.yayimorphology.nan.yayi;

import org.yayimorphology.nan.common.Utils;

import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;

public class ClangLintingJob extends YayiJob {

    public final static String clangVersion = "10.0.0";

    public ClangLintingJob(Job bambooJob) {
        super(bambooJob);
        requiresDocker = true;
        dockerFile = "tmp/dockerfile-clang";
    }

    @Override
    public void build() {

        ClangUtils clangUtils = new ClangUtils(clangVersion);
        this.cacheUtils.createCacheFolderTasks(bambooJob); // needed?
        // this.conanUtils.createConanPrepareTasks(bambooJob);// needed? no kept as an
        // example

        String scriptDockerCreate = clangUtils.bashScriptToGetDockerFile(dockerFile);

        this.bambooJob.tasks(
                new ScriptTask()
                        .description("Create Dockerfile for clang")
                        .interpreterShell()
                        .inlineBody(
                                Utils.multilineString(
                                        Utils.get_shebang(), // should be available on PATH
                                        "set -e",
                                        "",
                                        scriptDockerCreate)));

        // now able to construct the docker
        this.docker.prepareStartDockerPipeline(this.bambooJob);
        this.bambooJob.tasks(
                new ScriptTask()
                        .description("Wait for docker to be ready")
                        .interpreterShell()
                        .inlineBody(this.docker.get_docker_ready_script()));

        String script = Utils.multilineString(
                clangUtils.bashScriptToGetDockerFilePATHSettingsInDocker(),
                "",
                "cd " + this.getSourcecodeFolder(),
                "",
                Utils.echoInfoMessage("Running clang-format on the source tree"),
                "find . -type f \\",
                "    \\( -name \"*.cpp\" -or \\",
                "        -name \"*.h\" -or \\",
                "        -name \"*.hpp\" \\) \\",
                "    -not -path \"*/external_libraries/*\" \\",
                "    -not -path \"*/plugins/*\" \\",
                "    -exec clang-format \\",
                "          --style=file \\",
                "          -i \\",
                "          {} \\",
                "          +",
                "clang-format --help");

        addTask("Run clang-format", script);

        this.bambooJob.tasks(
                new ScriptTask()
                        .description("Interpret clang-format output")
                        .interpreterShell()
                        .inlineBody(
                                Utils.multilineString(
                                        Utils.get_shebang(),
                                        // "set -e", // not setting otherwise will fail on diffs
                                        "",
                                        Utils.echoInfoMessage("Checking clang-format results"),
                                        "cd " + this.getSourcecodeFolder(),
                                        "diff_content=$(git diff --exit-code)",
                                        "exit_code=$?",
                                        "if [ \"$exit_code\" -ne \"0\" ] ; then",
                                        "    (>&2 echo \"***** CLANG FORMAT FAILED *****\")",
                                        "    (>&2 echo )",
                                        "    (>&2 echo \"***** Summary of changes\")",
                                        "    # git diff --compact-summary not available on 16.04",
                                        "    (>&2 git status --short)",
                                        "    (>&2 echo )",
                                        "    (>&2 echo \"***** Complete diff\")",
                                        "    (>&2 git diff)",
                                        "    exit 1",
                                        "fi",
                                        "",
                                        Utils.echoInfoMessage("Running clang-format - end"))));

    }

}
