package org.yayimorphology.nan.yayi;

import org.yayimorphology.nan.common.Python3Utils;
import org.yayimorphology.nan.common.Utils;

import com.atlassian.bamboo.specs.api.builders.plan.Job;

public class PythonLintingJob extends YayiJob {

    String flakeConfig = "plugins/flake8.conf";
    String autoPep8Config = "plugins/autopep8.conf";
    private String virtualEnvLinting = "tmp/venv_linting";

    // Using the following adhoc flake8 configuration instead
    // [flake8]
    // ignore=E501,W503
    // exclude=.git,__pycache__,docs/source/conf.py,old,build,dist
    // max-complexity=10

    // Using the following for the autopep8
    // [pycodestyle]
    // ignore = E501
    // max-line-length=160

    public PythonLintingJob(Job bambooJob) {
        super(bambooJob);
    }

    @Override
    public void configureBuild() {
        // no need for cache (maybe except for pip packages) or anything else
        // complicated so we keep the main object uninitialized.
    }

    @Override
    public void build() {

        String script_skeleton = Utils.multilineString(
                // venv
                Utils.echoInfoMessage(
                        "creating and configuring the python virtual environment for python linting"),
                Python3Utils.scriptVirtualEnvInstall(
                        this.connection,
                        this.virtualEnvLinting),
                // this can be made optional and taken from the source folder
                // instead
                Utils.echoInfoMessage(
                        "adding linting tools to the virtual env"),
                "pip install -U autopep8 flake8",
                "",
                Utils.echoInfoMessage("Installed python packages"),
                "pip list");

        addTask("Install autopep8 and flake", script_skeleton);

        String commonVirtualEnvInstallationPreTask = Utils.multilineString(
                "# saving the base directory",
                "export base_dir=`pwd`",
                "",
                "# entering the virtual env",
                Python3Utils.getPythonVirtualenvActivationScript(virtualEnvLinting),
                "");

        String script = Utils.multilineString(
                "cd " + getSourcecodeFolder(),
                "has_failures=false",
                "",
                "# calling flake8",
                "flake8 \\",
                "    --statistics \\",
                "    --config=" + flakeConfig + " \\",
                "    --exit-zero \\",
                "    --tee \\",
                "    --output-file=$base_dir/tmp/flake8.out . 2>&1",
                "if [[ -s $base_dir/tmp/flake8.out ]]; then",
                "    (>&2 echo \"***** FLAKE FAILED *****\")",
                "    (>&2 echo )",
                "    (>&2 echo \"***** Report\")",
                "    (>&2 cat $base_dir/tmp/flake8.out)",
                "    has_failures=true",
                "fi",
                "",
                "echo",
                "echo",
                "",
                "# calling the linter",
                "find . \\",
                "     -type f \\( -name \"*.py\" \\) \\",
                "     -not -path \"*/build*\" \\",
                "     -not -path \"*/project_management*\" \\",
                "     -exec \"autopep8\" \\",
                "     --global-config=" + autoPep8Config + " \\",
                "     -i \\",
                "     {} +",
                "# checking the output",
                "if [[ `git status --porcelain` ]]; then",
                "    (>&2 echo \"***** AUTOPEP8 FORMAT FAILED *****\")",
                "    (>&2 echo )",
                "    (>&2 echo \"***** Summary of changes\")",
                "    # git diff --compact-summary not available on 16.04",
                "    (>&2 git status --short)",
                "    (>&2 echo )",
                "    (>&2 echo \"***** Complete diff\")",
                "    (>&2 git diff)",
                "    has_failures=true",
                "fi",
                "",
                "",
                "if [[ \"$has_failures\" == true ]]; then",
                "    exit 1",
                "fi",
                "");

        addTask("Run autopep8 and flake", commonVirtualEnvInstallationPreTask + script);
    }

}
