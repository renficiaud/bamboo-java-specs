package org.yayimorphology.nan.yayi;

import org.yayimorphology.nan.common.Utils;

public class ClangUtils {
    String clangVersion;

    /**
     *
     * @param clangVersion version of Clang required for installation
     */
    ClangUtils(String clangVersion) {
        this.clangVersion = clangVersion;
    }

    /**
     * Returns a bash scripts that creates a docker file that installs Clang. The
     * resulting docker file content is dynamically adapted to the running platform
     * to get the correct version of Clang.
     *
     *
     * @param dockerFile the location of the docker file the bash script should
     *                   create.
     * @return a bash script that creates dynamically the docker file.
     */
    public String bashScriptToGetDockerFile(String dockerFile) {
        String ubuntu1804_x86_64 = String.format("clang+llvm-%s-x86_64-linux-gnu-ubuntu-18.04", clangVersion);
        String ubuntu1804_arm7 = String.format("clang+llvm-%s-armv7a-linux-gnueabihf", clangVersion);

        String urlBase = String.format(
                "https://github.com/llvm/llvm-project/releases/download/llvmorg-%s/",
                clangVersion);

        String dockerFileContent = Utils.multilineString(
                "FROM ubuntu:18.04",
                "RUN apt-get update && \\",
                "    apt-get install -y wget xz-utils",
                "",
                "RUN cd /tmp && \\",
                "    wget $urlClang && \\",
                "    tar xf $archiveClang && \\",
                "    mv ${archiveClangBasename} /usr/local/" + String.format("clang-%s", clangVersion) + "/ && \\",
                "    rm ${archiveClang}");

        String scriptDockerCreate = Utils.multilineString(
                "case `uname -m` in ",
                "    \"armv7l\"*)",
                "        archiveClangBasename=\"" + ubuntu1804_arm7 + "\"",
                "        archiveExtension=\".tar.xz\"",
                "        ;;",
                "    \"x86_64\"*)",
                "        archiveClangBasename=\"" + ubuntu1804_x86_64 + "\"",
                "        archiveExtension=\".tar.xz\"",
                "        ;;",
                "esac",
                "",
                "export archiveClang=${archiveClangBasename}${archiveExtension}",
                String.format("export urlClang=%s${archiveClang}", urlBase),
                "",
                "mkdir -p tmp/",
                "cat <<EOF > " + dockerFile,
                dockerFileContent,
                "",
                "EOF",
                "",
                Utils.echoInfoMessage("Docker file content"),
                "cat " + dockerFile);
        return scriptDockerCreate;
    }

    public String bashScriptToGetDockerFilePATHSettingsInDocker() {
        return "export PATH=/usr/local/" + String.format("clang-%s", clangVersion) + "/bin:$PATH";
    }
}
