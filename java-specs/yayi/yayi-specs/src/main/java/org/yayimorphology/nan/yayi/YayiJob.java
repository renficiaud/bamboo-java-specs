package org.yayimorphology.nan.yayi;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.yayimorphology.nan.common.CacheUtils;
import org.yayimorphology.nan.common.ConanUtils;
import org.yayimorphology.nan.common.DockerUtils;
import org.yayimorphology.nan.common.Utils;
import org.yayimorphology.nan.common.Utils.BuildHost;

import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;

public abstract class YayiJob {
    /**
     *
     */
    protected Utils.ConnectionType connection = Utils.ConnectionType.local;
    // Utils.BuildHost host = Utils.BuildHost.linux;
    Job bambooJob;
    String checkoutFolder;

    ConanUtils conanUtils;
    CacheUtils cacheUtils;
    DockerUtils docker;

    boolean requiresDocker = false;
    protected String dockerFile;
    protected ArrayList<String> dockerArgs = new ArrayList<String>();
    protected List<Pair<String, String>> dockerMountLocation = new ArrayList<Pair<String, String>>();

    boolean requiresConan = false;

    /// Additional docker arguments.
    protected ArrayList<String> additionalDockerArguments = null;

    YayiJob(Job bambooJob) {
        this.bambooJob = bambooJob;
        this.cacheUtils = new CacheUtils();
        // the default cache goes to the machine defined one
        this.cacheUtils
                .setCacheFolderBase(
                        "${bamboo.capability.default_cache_folder}");
    }

    public void setSourcecodeFolder(String checkoutFolder) {
        this.checkoutFolder = checkoutFolder;
    }

    public String getSourcecodeFolder() {
        return this.checkoutFolder;
    }

    public void configureBuild() {
        connection = Utils.ConnectionType.local;
        if (this.requiresDocker) {
            connection = Utils.ConnectionType.docker;
        }

        // as soon as we run docker, we consider that the "real" stuff
        // will run inside docker. Conan is for the "real" stuff so it
        // inherits that connection.
        this.conanUtils = new ConanUtils(connection);

        // the conan cache prefix points to the calculated cache
        this.conanUtils
                .setConanCachePrefix(
                        this.cacheUtils.getCacheFolderBambooVariable());
        this.dockerMountLocation
                .addAll(this.conanUtils.getDockerFolderMapping());

        // configure the docker tasks for building
        if (this.requiresDocker) {

            this.docker = new DockerUtils(
                    "yayi",
                    this.dockerFile,
                    this.dockerMountLocation,
                    this.additionalDockerArguments);

            for (String arg : this.dockerArgs) {
                this.docker.addDockerBuildArguments(arg);
            }

            this.docker.linkToDetachedContainer(true);
        }

    }

    /**
     * To be overridden in child classes
     *
     */
    abstract public void build();

    /**
     * Adds a script task to the execution context of this job*
     *
     * The execution context is either Docker (if the task relies on Docker) or
     * a
     * script directly running on the agent.
     *
     * @param taskTitle
     *            name of the task as it will be shown in Bamboo
     * @param scriptToAdd
     *            script to execute
     */
    public void addTask(String taskTitle, String scriptToAdd) {

        if (this.requiresDocker) {
            this.docker.addContainerTask(
                    this.bambooJob,
                    taskTitle,
                    Utils.multilineString(
                            Utils.get_shebang(BuildHost.linux), // inside docker, this is "linux"
                            "set -e",
                            "",
                            "cd " + DockerUtils.getDockerWorkingDirectory(),
                            scriptToAdd));
        } else {
            this.bambooJob.tasks(
                    new ScriptTask()
                            .description(taskTitle)
                            .interpreterShell()
                            .inlineBody(
                                    Utils.multilineString(
                                            Utils.get_shebang(), // should be available on PATH
                                            "set -e",
                                            "",
                                            scriptToAdd)));
        }

    }

    void setJobRequirements() {
        List<Requirement> requirements = new ArrayList<>();

        // requirements.add(yayiJob.python_requirement);
        // requirements.add(yayiJob.cmake_requirement);

        // python3 is installed on all agents, but the scripts are not made to
        // be run
        // on windows, in addition if it requires docker we restrict the run to
        // only on
        // linux.
        if (this.requiresDocker) {
            requirements.add(
                    Requirement.exists("system.docker.executable"));
        }

        if (false) {
            Requirement operatingSystemRequirement = new Requirement(
                    "operating_system");
            operatingSystemRequirement.matchValue("windows");
            operatingSystemRequirement.matchType(Requirement.MatchType.EQUALS);
            requirements.add(operatingSystemRequirement);

            // not implemented yet
            operatingSystemRequirement.matchValue("(osx|linux)");
            operatingSystemRequirement.matchType(Requirement.MatchType.MATCHES);
        }

        bambooJob.requirements(
                requirements.toArray(
                        new Requirement[0]));
    }

}
