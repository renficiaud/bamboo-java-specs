package org.yayimorphology.nan.cmake;

import org.junit.Test;

import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;

public class PlanSpecTest {
  @Test
  public void checkYourPlanOffline() throws PropertiesValidationException {
    Plan plan = new PlanSpec().plan();
    
    EntityPropertiesBuilders.build(plan);
  }
}
