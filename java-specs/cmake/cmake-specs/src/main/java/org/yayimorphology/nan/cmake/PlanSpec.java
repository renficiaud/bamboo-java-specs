package org.yayimorphology.nan.cmake;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.notification.Notification;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.AllOtherPluginsConfiguration;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.ConcurrentBuilds;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.repository.VcsChangeDetection;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.builders.trigger.AnyTrigger;
import com.atlassian.bamboo.specs.builders.notification.PlanFailedNotification;
import com.atlassian.bamboo.specs.builders.notification.UserRecipient;
import com.atlassian.bamboo.specs.builders.repository.git.GitRepository;
import com.atlassian.bamboo.specs.builders.task.CheckoutItem;
import com.atlassian.bamboo.specs.builders.task.CommandTask;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;
import com.atlassian.bamboo.specs.model.task.ScriptTaskProperties;
import com.atlassian.bamboo.specs.util.BambooServer;
import com.atlassian.bamboo.specs.util.MapBuilder;

@BambooSpec
public class PlanSpec {

    public Plan plan() {
        final Plan plan = new Plan(
                new Project().oid(new BambooOid("enif8928iiv6")).key(new BambooKey("OSC"))
                        .name("Open source contributions")
                        .description("Builds for open source projects the SW participated to"),
                "CMake - findmatlab-nightly-dashboard", new BambooKey("CMAK")).oid(new BambooOid("en8q0np0onbn"))
                        .description("Night build of cmake for the FindMatlab extension maintained by the SW")
                        .pluginConfigurations(new ConcurrentBuilds().useSystemWideDefault(false))
                        .stages(new Stage("Default Stage").jobs(new Job("OSX10.10 - xcode611", new BambooKey("OSX"))
                                .pluginConfigurations(new AllOtherPluginsConfiguration()
                                        .configuration(new MapBuilder().put("custom", new MapBuilder()
                                                .put("auto", new MapBuilder().put("regex", "").put("label", "").build())
                                                .put("buildHangingConfig.enabled", "false").put("ncover.path", "")
                                                .put("clover", new MapBuilder().put("path", "").put("license", "")
                                                        .put("integration", "custom").put("useLocalLicenseKey", "true")
                                                        .build())
                                                .build()).build()))
                                .tasks(new VcsCheckoutTask().description("Checkout Default Repository")
                                        .checkoutItems(
                                                new CheckoutItem().defaultRepository().path("dashboard/CMakeScripts"))
                                        .cleanCheckout(true),
                                        new ScriptTask().description("Create the dashboard specific action")
                                                .interpreter(ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE)
                                                .inlineBody(
                                                        "echo \"\n# Client maintainer: raffi.enficiaud@free.fr\nset(CTEST_SITE \\\"bambooagent.raffienficiaud\\\")\nset(CTEST_BUILD_NAME \\\"OSX10.10-xcode611-matlab2017a\\\")\nset(CTEST_BUILD_CONFIGURATION Debug)\nset(CTEST_CMAKE_GENERATOR \\\"Unix Makefiles\\\")\nset(CTEST_BUILD_FLAGS \\\"-j4\\\")\nset(CTEST_TEST_ARGS PARALLEL_LEVEL 4)\n\nset(dashboard_cache \\\"CMake_TEST_FindMatlab:BOOL=ON\\\")\n\ninclude(\\${CTEST_SCRIPT_DIRECTORY}/cmake_common.cmake)\n\" > findmatlab_nightbuild.cmake\n\n\nmore findmatlab_nightbuild.cmake << /dev/null")
                                                .workingSubdirectory("dashboard/CMakeScripts"),
                                        new CommandTask().description("Compilation and tests").executable("ctest")
                                                .argument("-S findmatlab_nightbuild.cmake -V")
                                                .workingSubdirectory("dashboard/CMakeScripts"))
                                .requirements(new Requirement("system.builder.command.matlab2017a"),
                                        new Requirement("system.builder.command.python2.7"),
                                        new Requirement("system.builder.command.cmake"),
                                        new Requirement("operating_system").matchValue("osx")
                                                .matchType(Requirement.MatchType.EQUALS))
                                .cleanWorkingDirectory(true),
                                new Job("Win7VS2017 - x64", new BambooKey("JOB1"))
                                        .pluginConfigurations(new AllOtherPluginsConfiguration()
                                                .configuration(new MapBuilder().put("custom", new MapBuilder()
                                                        .put("auto",
                                                                new MapBuilder().put("regex", "").put("label", "")
                                                                        .build())
                                                        .put("buildHangingConfig.enabled", "false")
                                                        .put("ncover.path", "")
                                                        .put("clover", new MapBuilder().put("path", "")
                                                                .put("license", "").put("integration", "custom")
                                                                .put("useLocalLicenseKey", "true").build())
                                                        .build()).build()))
                                        .tasks(new VcsCheckoutTask().description("Checkout Default Repository")
                                                .checkoutItems(new CheckoutItem().defaultRepository()
                                                        .path("dashboard/CMakeScripts"))
                                                .cleanCheckout(true),
                                                new ScriptTask().description("Create the dashboard specific action")
                                                        .interpreter(ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE)
                                                        .inlineBody(
                                                                "@echo off\necho # Client maintainer: raffi.enficiaud@free.fr > findmatlab_nightbuild.cmake\necho set(CTEST_SITE \"bambooagent.raffienficiaud\") >> findmatlab_nightbuild.cmake\necho set(CTEST_BUILD_NAME \"Win7x64-vs2017-matlab2013b\") >> findmatlab_nightbuild.cmake\necho set(CTEST_BUILD_CONFIGURATION Debug) >> findmatlab_nightbuild.cmake\necho set(CTEST_CMAKE_GENERATOR \"Visual Studio 15 2017 Win64\") >> findmatlab_nightbuild.cmake\necho set(CTEST_TEST_ARGS EXCLUDE VSExcludeFromDefaultBuild PARALLEL_LEVEL 2) >> findmatlab_nightbuild.cmake\n\necho.  >> findmatlab_nightbuild.cmake\necho set(dashboard_cache \"CMake_TEST_FindMatlab:BOOL=ON\") >> findmatlab_nightbuild.cmake\n\necho. >> findmatlab_nightbuild.cmake\necho include(${CTEST_SCRIPT_DIRECTORY}/cmake_common.cmake) >> findmatlab_nightbuild.cmake\n\ntype findmatlab_nightbuild.cmake")
                                                        .workingSubdirectory("dashboard/CMakeScripts"),
                                                new CommandTask().description("Compilation and tests")
                                                        .executable("ctest")
                                                        .argument("-S findmatlab_nightbuild.cmake -V")
                                                        .workingSubdirectory("dashboard/CMakeScripts"))
                                        .requirements(new Requirement("system.builder.command.python2.7"),
                                                new Requirement("system.builder.command.cmake"),
                                                new Requirement("operating_system").matchValue("win64")
                                                        .matchType(Requirement.MatchType.EQUALS),
                                                new Requirement("system.builder.command.matlab2013b"))
                                        .cleanWorkingDirectory(true),
                                new Job("Win10VS2017 - x64 - MCR", new BambooKey("WXM"))
                                        .description("MCR version of the build")
                                        .pluginConfigurations(new AllOtherPluginsConfiguration()
                                                .configuration(new MapBuilder().put("custom", new MapBuilder()
                                                        .put("auto", new MapBuilder().put("regex", "").put("label", "")
                                                                .build())
                                                        .put("buildHangingConfig.enabled", "false")
                                                        .put("ncover.path", "")
                                                        .put("clover", new MapBuilder().put("path", "")
                                                                .put("license", "").put("integration", "custom")
                                                                .put("useLocalLicenseKey", "true").build())
                                                        .build()).build()))
                                        .tasks(new VcsCheckoutTask().description("Checkout Default Repository")
                                                .checkoutItems(new CheckoutItem().defaultRepository()
                                                        .path("dashboard/CMakeScripts"))
                                                .cleanCheckout(true),
                                                new ScriptTask().description("Create the dashboard specific action")
                                                        .interpreter(ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE)
                                                        .inlineBody(
                                                                "@echo off\necho # Client maintainer: raffi.enficiaud@free.fr > findmatlab_nightbuild.cmake\necho set(CTEST_SITE \"bambooagent.raffienficiaud\") >> findmatlab_nightbuild.cmake\necho set(CTEST_BUILD_NAME \"Win10x64-vs2017-matlab2017a-mcr\") >> findmatlab_nightbuild.cmake\necho set(CTEST_BUILD_CONFIGURATION Debug) >> findmatlab_nightbuild.cmake\necho set(CTEST_CMAKE_GENERATOR \"Visual Studio 15 2017 Win64\") >> findmatlab_nightbuild.cmake\necho set(CTEST_TEST_ARGS EXCLUDE VSExcludeFromDefaultBuild PARALLEL_LEVEL 2) >> findmatlab_nightbuild.cmake\n\necho.  >> findmatlab_nightbuild.cmake\necho set(dashboard_cache \"CMake_TEST_FindMatlab_MCR:BOOL=ON\") >> findmatlab_nightbuild.cmake\n\necho. >> findmatlab_nightbuild.cmake\necho include(${CTEST_SCRIPT_DIRECTORY}/cmake_common.cmake) >> findmatlab_nightbuild.cmake\n\ntype findmatlab_nightbuild.cmake")
                                                        .workingSubdirectory("dashboard/CMakeScripts"),
                                                new CommandTask().description("Compilation and tests")
                                                        .executable("ctest")
                                                        .argument("-S findmatlab_nightbuild.cmake -V")
                                                        .workingSubdirectory("dashboard/CMakeScripts"))
                                        .requirements(new Requirement("mcr_runtime"),
                                                new Requirement("system.builder.command.python2.7"),
                                                new Requirement("system.builder.command.cmake"),
                                                new Requirement("operating_system").matchValue("win64")
                                                        .matchType(Requirement.MatchType.EQUALS))
                                        .cleanWorkingDirectory(true),
                                new Job("LTS16.04 - x64", new BambooKey("LX"))
                                        .pluginConfigurations(new AllOtherPluginsConfiguration()
                                                .configuration(new MapBuilder().put("custom", new MapBuilder()
                                                        .put("auto",
                                                                new MapBuilder().put("regex", "").put("label", "")
                                                                        .build())
                                                        .put("buildHangingConfig.enabled", "false")
                                                        .put("ncover.path", "")
                                                        .put("clover", new MapBuilder().put("path", "")
                                                                .put("license", "").put("integration", "custom")
                                                                .put("useLocalLicenseKey", "true").build())
                                                        .build()).build()))
                                        .tasks(new VcsCheckoutTask().description("Checkout Default Repository")
                                                .checkoutItems(new CheckoutItem().defaultRepository()
                                                        .path("dashboard/CMakeScripts"))
                                                .cleanCheckout(true),
                                                new ScriptTask().description("Create the dashboard specific action")
                                                        .interpreter(ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE)
                                                        .inlineBody(
                                                                "echo \"\n# Client maintainer: raffi.enficiaud@free.fr\nset(CTEST_SITE \\\"bambooagent.raffienficiaud\\\")\nset(CTEST_BUILD_NAME \\\"LTS16.04-matlab2017a\\\")\nset(CTEST_BUILD_CONFIGURATION Debug)\nset(CTEST_CMAKE_GENERATOR \\\"Unix Makefiles\\\")\nset(CTEST_BUILD_FLAGS \\\"-j8\\\")\nset(CTEST_TEST_ARGS PARALLEL_LEVEL 10)\n\nset(dashboard_cache \\\"\nCMake_TEST_FindMatlab:BOOL=ON\nCMake_TEST_CMakeOnly.AllFindModules_NO_VERSION:STRING=FREETYPE\\\")\n\ninclude(\\${CTEST_SCRIPT_DIRECTORY}/cmake_common.cmake)\n\" > findmatlab_nightbuild.cmake\n\n\nmore findmatlab_nightbuild.cmake << /dev/null")
                                                        .workingSubdirectory("dashboard/CMakeScripts"),
                                                new CommandTask().description("Compilation and tests")
                                                        .executable("ctest")
                                                        .argument("-S findmatlab_nightbuild.cmake -V")
                                                        .workingSubdirectory("dashboard/CMakeScripts"))
                                        .requirements(new Requirement("system.builder.command.python2.7"),
                                                new Requirement("operating_system").matchValue("linux")
                                                        .matchType(Requirement.MatchType.EQUALS),
                                                new Requirement("system.builder.command.cmake"),
                                                new Requirement("system.builder.command.matlab2013b"))
                                        .cleanWorkingDirectory(true),
                                new Job("OSX10.10 - xcode6.4 - MCR", new BambooKey("OX10"))
                                        .pluginConfigurations(new AllOtherPluginsConfiguration()
                                                .configuration(new MapBuilder().put("custom", new MapBuilder()
                                                        .put("auto",
                                                                new MapBuilder().put("regex", "").put("label", "")
                                                                        .build())
                                                        .put("buildHangingConfig.enabled", "false")
                                                        .put("ncover.path", "")
                                                        .put("clover", new MapBuilder().put("path", "")
                                                                .put("license", "").put("integration", "custom")
                                                                .put("useLocalLicenseKey", "true").build())
                                                        .build()).build()))
                                        .tasks(new VcsCheckoutTask().description("Checkout Default Repository")
                                                .checkoutItems(new CheckoutItem().defaultRepository()
                                                        .path("dashboard/CMakeScripts"))
                                                .cleanCheckout(true),
                                                new ScriptTask().description("Create the dashboard specific action")
                                                        .interpreter(ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE)
                                                        .inlineBody(
                                                                "echo \"\n# Client maintainer: raffi.enficiaud@free.fr\nset(CTEST_SITE \\\"bambooagent.raffienficiaud\\\")\nset(CTEST_BUILD_NAME \\\"OSX10.10-xcode64-matlab2017a-mcr\\\")\nset(CTEST_BUILD_CONFIGURATION Debug)\nset(CTEST_CMAKE_GENERATOR \\\"Unix Makefiles\\\")\nset(CTEST_BUILD_FLAGS \\\"-j2\\\")\nset(CTEST_TEST_ARGS PARALLEL_LEVEL 2)\n\nset(dashboard_cache \\\"CMake_TEST_FindMatlab_MCR:BOOL=ON\\\")\n\ninclude(\\${CTEST_SCRIPT_DIRECTORY}/cmake_common.cmake)\n\" > findmatlab_nightbuild.cmake\n\n\nmore findmatlab_nightbuild.cmake << /dev/null")
                                                        .workingSubdirectory("dashboard/CMakeScripts"),
                                                new CommandTask().description("Compilation and tests")
                                                        .executable("ctest")
                                                        .argument("-S findmatlab_nightbuild.cmake -V")
                                                        .workingSubdirectory("dashboard/CMakeScripts"))
                                        .requirements(new Requirement("mcr_runtime"),
                                                new Requirement("system.builder.command.python2.7"),
                                                new Requirement("system.builder.command.cmake"),
                                                new Requirement("operating_system").matchValue("osx")
                                                        .matchType(Requirement.MatchType.EQUALS))
                                        .cleanWorkingDirectory(true),
                                new Job("LTS16.04 - x64 - MCR", new BambooKey("LXM"))
                                        .description("MCR version of the build")
                                        .pluginConfigurations(new AllOtherPluginsConfiguration()
                                                .configuration(new MapBuilder().put("custom", new MapBuilder()
                                                        .put("auto", new MapBuilder().put("regex", "").put("label", "")
                                                                .build())
                                                        .put("buildHangingConfig.enabled", "false")
                                                        .put("ncover.path", "")
                                                        .put("clover", new MapBuilder().put("path", "")
                                                                .put("license", "").put("integration", "custom")
                                                                .put("useLocalLicenseKey", "true").build())
                                                        .build()).build()))
                                        .tasks(new VcsCheckoutTask().description("Checkout Default Repository")
                                                .checkoutItems(new CheckoutItem().defaultRepository()
                                                        .path("dashboard/CMakeScripts"))
                                                .cleanCheckout(true),
                                                new ScriptTask().description("Create the dashboard specific action")
                                                        .interpreter(ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE)
                                                        .inlineBody(
                                                                "echo \"\n# Client maintainer: raffi.enficiaud@free.fr\nset(CTEST_SITE \\\"bambooagent.raffienficiaud\\\")\nset(CTEST_BUILD_NAME \\\"LTS16.04-matlab2017a-mcr\\\")\nset(CTEST_BUILD_CONFIGURATION Debug)\nset(CTEST_CMAKE_GENERATOR \\\"Unix Makefiles\\\")\nset(CTEST_BUILD_FLAGS \\\"-j8\\\")\nset(CTEST_TEST_ARGS PARALLEL_LEVEL 10)\n\nset(dashboard_cache \\\"\nCMake_TEST_FindMatlab_MCR_ROOT_DIR:FILEPATH=${bamboo.capability.mcr_runtime_path}\nCMake_TEST_CMakeOnly.AllFindModules_NO_VERSION:STRING=FREETYPE\\\")\n\ninclude(\\${CTEST_SCRIPT_DIRECTORY}/cmake_common.cmake)\n\" > findmatlab_nightbuild.cmake\n\n\nmore findmatlab_nightbuild.cmake << /dev/null")
                                                        .workingSubdirectory("dashboard/CMakeScripts"),
                                                new CommandTask().description("Compilation and tests")
                                                        .executable("ctest")
                                                        .argument("-S findmatlab_nightbuild.cmake -V")
                                                        .workingSubdirectory("dashboard/CMakeScripts"))
                                        .requirements(new Requirement("mcr_runtime"),
                                                new Requirement("system.builder.command.python2.7"),
                                                new Requirement("operating_system").matchValue("linux")
                                                        .matchType(Requirement.MatchType.EQUALS),
                                                new Requirement("system.builder.command.cmake"))
                                        .cleanWorkingDirectory(true)))
                        .planRepositories(
                                new GitRepository().name("cmake @ cmake.org").oid(new BambooOid("encvohf96k23"))
                                        .url("git://cmake.org/cmake.git").branch("dashboard").shallowClonesEnabled(true)
                                        .remoteAgentCacheEnabled(false).changeDetection(new VcsChangeDetection()))

                        .triggers(new AnyTrigger(
                                new AtlassianModule("com.atlassian.bamboo.triggers.atlassian-bamboo-triggers:daily"))
                                        .name("Single daily build").description("Night build at 5AM")
                                        .configuration(new MapBuilder()
                                                .put("repository.change.daily.buildTime", "05:00").build()))
                        .planBranchManagement(
                                new PlanBranchManagement().delete(new BranchCleanup()).notificationForCommitters())
                        .notifications(new Notification().type(new PlanFailedNotification())
                                .recipients(new UserRecipient("renficiaud")));
        return plan;
    }

    public PlanPermissions planPermission() {
        final PlanPermissions planPermission = new PlanPermissions(new PlanIdentifier("OSC", "CMAK"))
                .permissions(new Permissions()
                        .userPermissions("renficiaud", PermissionType.VIEW, PermissionType.BUILD, PermissionType.CLONE,
                                PermissionType.EDIT, PermissionType.ADMIN)
                        .loggedInUserPermissions(PermissionType.VIEW).anonymousUserPermissionView());
        return planPermission;
    }

    public static void main(String... argv) {
        // By default credentials are read from the '.credentials' file.
        BambooServer bambooServer = new BambooServer("https://atlas.is.localnet/bamboo");
        final PlanSpec planSpec = new PlanSpec();

        final Plan plan = planSpec.plan();
        bambooServer.publish(plan);

        final PlanPermissions planPermission = planSpec.planPermission();
        bambooServer.publish(planPermission);
    }
}